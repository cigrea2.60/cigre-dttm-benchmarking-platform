# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import datetime as dt
import warnings
import os

from matplotlib.ticker import PercentFormatter, FormatStrFormatter
import matplotlib.pyplot as plt
plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams["font.size"] = 10
plt.close("all")

from openpyxl import load_workbook

class dotdict(dict):
    """
    dot.notation access to dictionary attributes
    """
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

TIMESERIES_NAME = ['time', 'load', 't_amb', 't_tl', 't_hs', 'cooling_stage']

DICT_AGEING_PARAMS = {}
DICT_AGEING_PARAMS["not-tup"] = {}
DICT_AGEING_PARAMS["not-tup"]["Free from air and 0.5 % moisture"] = {"A": 4.1e10, "Ea": 128000}
DICT_AGEING_PARAMS["not-tup"]["Free from air and 1.5 % moisture"] = {"A": 1.5e11, "Ea": 128000}
DICT_AGEING_PARAMS["not-tup"]["Free from air and 3.5 % moisture"] = {"A": 4.5e11, "Ea": 128000}
DICT_AGEING_PARAMS["not-tup"]["With air and 0.5 % moisture"] = {"A": 4.6e5, "Ea": 89000}
DICT_AGEING_PARAMS["tup"] = {}
DICT_AGEING_PARAMS["tup"]["Free from air and 0.5 % moisture"] = {"A": 1.6e4, "Ea": 86000}
DICT_AGEING_PARAMS["tup"]["Free from air and 1.5 % moisture"] = {"A": 3.0e4, "Ea": 86000}
DICT_AGEING_PARAMS["tup"]["Free from air and 3.5 % moisture"] = {"A": 6.1e4, "Ea": 86000}
DICT_AGEING_PARAMS["tup"]["With air and 0.5 % moisture"] = {"A": 3.2e4, "Ea": 82000}
DICT_AGEING_RATED_TEMP = {"tup": 110.0, "not-tup": 98.0}

DPI = 120

class TTS:
    """
    Transformer Thermal Scenario
    """

    def __init__(self, id, path_parameters, path_timeserie, ws_params_name='TTS parameters'):
        """
        Class constructor
        """
        self.id = id
        self.path_parameters = path_parameters
        self.path_timeserie = path_timeserie
        self.path_tts = os.path.dirname(self.path_parameters)

        # Load parameters
        self.ws_params_name = ws_params_name
        self.params = {}
        self.loadParameters()

        # Load timeserie
        self.ts_original = pd.DataFrame()
        self.loadTimeserie()

        # Resampled timeserie (if data isn't resampled, there is a risk of having NaN values due to the different timeseries in the Excel files)
        self.ts_resampled = pd.DataFrame()

    def loadParameters(self):
        """
        Load TTS parameters from Excel file
        """
        print("Load parameters from Excel file: %s"%self.path_parameters)
        # Import Workbook
        warnings.simplefilter(action='ignore', category=UserWarning)
        wb = load_workbook(self.path_parameters, data_only=True)

        # Load Worksheet 'TTS parameters'
        if not self.ws_params_name in wb.sheetnames:
            raise Exception("Worksheet %s is not present in file %s"%(self.ws_params_name, self.path_parameters))
        ws = wb[self.ws_params_name]

        # Data columns and rows
        k_row_start_data = -1
        for row in ws.iter_rows():
            for cell in row:
                if cell.value in ["longname", "shortname"]:
                    k_row_start_data = cell.row + 1 # Data begining
        if k_row_start_data < 0:
            raise Exception("Problem with file %s data, check headers"%self.path_parameters)

        dict_column = {}
        for col in ws.iter_cols(min_row=k_row_start_data-1, max_row=k_row_start_data-1):
            for cell in col:
                dict_column[cell.value] = cell.column

        for header in ["longname", "unit", "shortname", "value", "to be distributed to proprietary DTTM", "models"]:
            if not header in dict_column.keys():
                raise Exception("Problem with file %s data, check header %s"%(self.path_parameters, header))
        dict_column["to_distribute"] = dict_column.pop("to be distributed to proprietary DTTM")

        # Load data
        k_column_min = min([dict_column[header] for header in dict_column.keys()])
        k_column_max = max([dict_column[header] for header in dict_column.keys()])
        for header in dict_column.keys():
            dict_column[header] = dict_column[header] - k_column_min

        for row in ws.iter_rows(min_row=k_row_start_data, max_row=None, min_col=k_column_min, max_col=k_column_max):

            if not row[dict_column["shortname"]].value is None:

                name = row[dict_column["shortname"]].value
                if row[dict_column["models"]].value is None:
                    lst_model = ['main']
                else:
                    lst_model = row[dict_column["models"]].value.split(",")

                for model in lst_model:
                    if not model in self.params.keys():
                        self.params[model] = {}
                    if name in self.params[model].keys():
                        raise Exception("Several variables with shortname %s for model %s"%(name, model))

                    self.params[model][name] = {}
                    for header in dict_column.keys():
                        if not header in ["shortname", "models"]:
                            self.params[model][name][header] = row[dict_column[header]].value
                    self.params[model][name] = dotdict(self.params[model][name])

        for model in self.params.keys():
            self.params[model] = dotdict(self.params[model])
        self.params = dotdict(self.params)

    def loadTimeserie(self):
        """
        Load TTS timeseries from Excel file
        """
        print("Load timeseries from Excel file: %s"%self.path_timeserie)
        # Load all Excel sheets
        df_dict = pd.read_excel(self.path_timeserie, index_col=None, sheet_name=None)

        # Merge sheets base on time, if no value available then it's NaN value
        for k, sheet_name in enumerate(df_dict.keys()):
            print("- Load sheet %s"%sheet_name)
            df_tmp = df_dict[sheet_name]

            # Remove unnamed columns
            df_tmp.drop(df_tmp.columns[df_tmp.columns.str.contains('unnamed', case=False)], axis = 1, inplace = True)

            # Check columns name
            if not "time" in df_tmp.columns:
                raise Exception("No time column in worksheet %s"%sheet_name)

            # Merge in one dataframe
            if k == 0:
                self.ts_original = df_tmp.copy()
            else:
                self.ts_original = pd.merge_ordered(self.ts_original, df_tmp, fill_method=None, on="time")

        print("Columns name: %s"%list(self.ts_original.columns))

        # Check the names that are provided
        for name in TIMESERIES_NAME:
            if not name in self.ts_original.columns:
                print("%s is not provided"%name)

        # Check if there is duplicated timestamps
        tmp = self.ts_original[self.ts_original.duplicated(subset=['time'], keep=False)]
        if len(tmp) > 0:
            print("There is duplicated timestamps in the timeseries!")
            print(tmp)
            raise Exception("Remove the duplicated timestamps in the timeseries file.")

    def saveTimeserieToExcel(self, ts, path_out):
        """
        Save timeserie to Excel file
        """
        writer = pd.ExcelWriter(path_out, engine='xlsxwriter', datetime_format="YYYY-MM-DD HH:MM:SS.000")
        ts.to_excel(writer, index=False)
        writer.close()
        print("Timeseries saved to Excel file: %s"%path_out)

    def resampleTimeserie(self, new_time_step):
        """
        Resample the timeserie at newTimeStep (in seconds)
        Return the resampled timeserie
        """
        # New time array, including the last value
        ts = np.arange(self.ts_original.time.min().to_pydatetime(), self.ts_original.time.max().to_pydatetime() + dt.timedelta(seconds=new_time_step), new_time_step*1000000)
        df_ts_new = pd.DataFrame({'time': ts})

        # Add new timestamps
        df_tmp = pd.merge_ordered(df_ts_new, self.ts_original, fill_method=None, on="time")

        # Linear interpolation based on timestamps
        df_tmp.set_index('time', inplace=True)
        df_tmp.interpolate(method='index', inplace=True)
        df_tmp.reset_index(inplace=True)
        df_tmp.fillna(method="bfill", inplace=True)

        # Extraction at the requested timestamps
        self.ts_resampled = df_tmp[df_tmp.time.isin(df_ts_new.time)].copy()
        self.ts_resampled.reset_index(inplace=True, drop=True)

    def plot(self, ts, label_title="",
             lst_first_axis=[], label_first_axis="", y_lim_first_axis={}, mpl_options_first_axis={},
             lst_second_axis=[], label_second_axis="", y_lim_second_axis={}, mpl_options_second_axis={},
             lst_third_axis=[], label_third_axis="", y_lim_third_axis={}, mpl_options_third_axis={},
             emphasize_time=None):
        """
        Plot timeserie
        """
        # Check if names are in the timeseries
        for key in lst_first_axis.copy():
            if not key in ts.keys():
                if key in self.ts_resampled.keys():
                    print("Key %s is not in timeserie => Take from resampled timeserie"%key)
                    ts = pd.merge_ordered(ts, self.ts_resampled[["time", key]], fill_method=None, on="time")
                else:
                    print("Key %s is not in timeseries => Remove key"%key)
                    lst_first_axis.remove(key)
        for key in lst_second_axis.copy():
            if not key in ts.keys():
                if key in self.ts_resampled.keys():
                    print("Key %s is not in timeserie => Take from resampled timeserie"%key)
                    ts = pd.merge_ordered(ts, self.ts_resampled[["time", key]], fill_method=None, on="time")
                else:
                    print("Key %s is not in timeseries => Remove key"%key)
                    lst_second_axis.remove(key)
        for key in lst_third_axis.copy():
            if not key in ts.keys():
                if key in self.ts_resampled.keys():
                    print("Key %s is not in timeserie => Take from resampled timeserie"%key)
                    ts = pd.merge_ordered(ts, self.ts_resampled[["time", key]], fill_method=None, on="time")
                else:
                    print("Key %s is not in timeseries => Remove key"%key)
                    lst_third_axis.remove(key)

        if len(lst_third_axis) > 0 and len(lst_second_axis) == 0:
            lst_second_axis = lst_third_axis
            label_second_axis = label_third_axis
            lst_third_axis = []
            label_third_axis = ""

        # For 3 axis
        def make_format(first, second, third):
            def format_coord(x, y):
                display_coord = first.transData.transform((x,y))
                inv1 = first.transData.inverted()
                inv2 = second.transData.inverted()
                inv3 = third.transData.inverted()
                ax_coord1 = inv1.transform(display_coord)
                ax_coord2 = inv2.transform(display_coord)
                ax_coord3 = inv3.transform(display_coord)
                return "1st axis=%.2f 2nd axis=%.2f 3rd axis=%.2f"%(ax_coord2[1], ax_coord1[1], ax_coord3[1])
            return format_coord

        # Plot
        ts.set_index('time', inplace=True)

        fig, ax = plt.subplots(figsize=(6.4, 5.5), dpi=DPI)
        if len(lst_third_axis) > 0:
            ax3 = ax.twinx()
            rspine = ax3.spines['right']
            rspine.set_position(('axes', 1.15))
            ax3.set_frame_on(True)
            ax3.patch.set_visible(False)
            fig.subplots_adjust(right=0.7)

        lns = []
        if len(lst_first_axis) > 0:
            ts[lst_first_axis].plot(ax=ax, legend=False, xlabel="", x_compat=True)
            lns += ax.get_lines()
        if len(lst_second_axis) > 0:
            ts[lst_second_axis].plot(ax=ax, secondary_y=True, style="--", legend=False, mark_right=False, xlabel="", x_compat=True)
            lns += ax.right_ax.get_lines()
        if len(lst_third_axis) > 0:
            ts[lst_third_axis].plot(ax=ax3, style="-.", legend=False, xlabel="", x_compat=True)
            lns += ax3.get_lines()

        # Emphazis areas
        if not emphasize_time is None:
            ax.fill_between(ts.index, 0, 1, where=pd.Series(ts.index.isin(emphasize_time)), color='green', alpha=0.2, transform=ax.get_xaxis_transform())

        # Rename labels
        labels = []
        for ln in lns:
            lbl = ln.get_label()
            if lbl in self.params.main.keys():
                if not self.params.main[lbl].longname is None:
                    lbl = self.params.main[lbl].longname
            labels.append(lbl)

        if len(labels)%2 == 0:
            nrow = int(len(labels)/2)
        else:
            nrow = int(len(labels)/2) + 1
        plt.legend(lns, labels, bbox_to_anchor =(0.5,-0.1*nrow), loc='lower center', ncol=2, facecolor='white', framealpha=0.8)

        if len(lst_first_axis) > 0:
            ax.set_ylabel(label_first_axis)
            if "major_formatter" in mpl_options_first_axis.keys():
                ax.yaxis.set_major_formatter(mpl_options_first_axis["major_formatter"])
            if "major_locator" in mpl_options_first_axis.keys():
                ax.yaxis.set_major_locator(mpl_options_first_axis["major_locator"])
            ax.set_ylim(**y_lim_first_axis)
        if len(lst_second_axis) > 0:
            ax.right_ax.set_ylabel(label_second_axis)
            if "major_formatter" in mpl_options_second_axis.keys():
                ax.right_ax.yaxis.set_major_formatter(mpl_options_second_axis["major_formatter"])
            if "major_locator" in mpl_options_second_axis.keys():
                ax.right_ax.yaxis.set_major_locator(mpl_options_second_axis["major_locator"])
            ax.right_ax.set_ylim(**y_lim_second_axis)
        if len(lst_third_axis) > 0:
            ax3.set_ylabel(label_third_axis)
            if "major_formatter" in mpl_options_third_axis.keys():
                ax3.yaxis.set_major_formatter(mpl_options_third_axis["major_formatter"])
            if "major_locator" in mpl_options_third_axis.keys():
                ax3.yaxis.set_major_locator(mpl_options_third_axis["major_locator"])
            ax3.set_ylim(**y_lim_third_axis)

        ax.set_title(label_title)
        fig.canvas.manager.set_window_title(label_title)
        ax.grid('on', which='both', axis='both')
        ax.text(0.005, 0.95, self.id, transform=ax.transAxes, color='gray', alpha=0.75, ha='left', va='bottom')

        if len(lst_third_axis) > 0:
            ax.right_ax.format_coord = make_format(ax.right_ax, ax, ax3)

        fig.tight_layout()

        ts.reset_index(inplace=True)

        return fig

    def differencesEvaluation(self, df_tocompare, analysisToPerform=[], xlsx_writer=None, label_title='differences', path_out=None):
        """
        Evaluate the differences with the TTS
        """
        # Keep only DTTM with more than one value (prevent an error for not filled Proprietary DTTM)
        df_tocompare = df_tocompare.loc[:, df_tocompare.count()>1]

        # Error
        df_error = df_tocompare.copy()
        df_error.drop(["time", "Measured"], axis=1, inplace=True)
        df_error = df_error.subtract(df_tocompare.Measured, axis=0)
        if df_error.empty:
            return

        ### Metrics
        # Percentiles
        #df_metrics = df_error.describe(percentiles=[0.01, 0.05, 0.25, 0.50, 0.75, 0.95, 0.99])

        ### Tim's simplification
        df_metrics = df_error.describe(percentiles=None)
        df_metrics.drop(["count", "min", "max", "25%", "50%", "75%"], inplace = True)
        ###

        # RMSE
        rmse = np.sqrt(np.mean(np.power(df_error, 2), axis=0))
        df_metrics = pd.concat((df_metrics, pd.DataFrame({'rmse': rmse}).transpose()), axis=0)

        # Ageing rate weighted (has only a meaning for hot-spot) - IEC 60076-7:2017 §A.2
        if "ageing" in analysisToPerform:
            if not(self.params.main.typePaper is None or self.params.main.ageingParameters is None):
                typePaper = self.params.main.typePaper.value
                ageingParameters = self.params.main.ageingParameters.value
                def ageingSpeed(ts):
                    if ageingParameters == "Standard (no air and moisture consideration)":
                        # IEC 60076-7:2017 §6.3
                        if typePaper == "tup":
                            v = np.exp(15000./(110+273) - 15000/(ts + 273))
                        else:
                            v = np.power(2, (ts - 98)/6)
                    else:
                        # IEC 60076-7:2017 §A.2
                        rated_ageing_params = "Free from air and 0.5 % moisture"
                        R = 8.314
                        v = DICT_AGEING_PARAMS[typePaper][ageingParameters]["A"]/DICT_AGEING_PARAMS[typePaper][rated_ageing_params]["A"]*\
                            np.exp((1/R)*(DICT_AGEING_PARAMS[typePaper][rated_ageing_params]["Ea"]/(DICT_AGEING_RATED_TEMP[typePaper] + 273) -\
                                        DICT_AGEING_PARAMS[typePaper][ageingParameters]["Ea"]/(ts + 273)))
                    return v
                v_reference = ageingSpeed(df_tocompare.Measured)
                L_reference = (v_reference*df_tocompare.time.diff().dt.total_seconds()).sum() # seconds

                res = {'avg(v_dttm/v_tts)': [],
                        'avg(if(v_dttm/v_tts > 1))': [],
                        'L_dttm/L_tts': []}
                for name in df_error.columns:
                    v_tocompare = ageingSpeed(df_tocompare[name])
                    L_tocompare = (v_tocompare*df_tocompare.time.diff().dt.total_seconds()).sum() # seconds

                    tmp = v_tocompare/v_reference
                    res['avg(v_dttm/v_tts)'].append(np.mean(tmp))
                    res['avg(if(v_dttm/v_tts > 1))'].append(np.mean(tmp.loc[tmp>1]))
                    res['L_dttm/L_tts'].append(L_tocompare/L_reference)

                df_metrics = pd.concat((df_metrics, pd.DataFrame(res, index=df_error.columns).transpose()), axis=0)

        # Save metrics to Excel
        df_metrics = pd.concat((df_metrics, pd.DataFrame({'count': df_error.count()}).transpose()), axis=0) # To get the count at the end
        if not xlsx_writer is None:
            if " - " in label_title:
                df_metrics.to_excel(xlsx_writer, sheet_name=label_title[label_title.index(" - ")+3:], float_format = "%.2f")
            else:
                df_metrics.to_excel(xlsx_writer, sheet_name=label_title[:30], float_format = "%.2f")

        ### Graphs
        maxbins = 100
        minbinwidth = 0.5 #°C

        if "comparison_graph" in analysisToPerform:
            ## Error distribution
            fig = plt.figure(dpi=DPI)
            ax = fig.add_subplot(1, 1, 1)
            weights = np.ones_like(df_error) / df_error.size
            numbins = max(1, min((((df_error.values.max() - df_error.values.min())/minbinwidth).astype(int)+1).max(), maxbins))
            binwidth = (df_error.values.max() - df_error.values.min())/numbins
            bins = np.arange(df_error.values.min(), df_error.values.max() + binwidth, binwidth)

            lst_p = []
            for k, c in enumerate(df_error.columns):
                values, bins, p = ax.hist(df_error[c], bins=bins, weights=weights[:,k], label=c)
                area_tmp = sum(np.diff(bins)*values)
                lst_p.append({"p": p, "area": area_tmp})
            lst_p = sorted(lst_p, key=lambda d: d['area'], reverse=True)
            for k, d in enumerate(lst_p):
                for pp in d["p"]:
                    pp.set_zorder(k+1)
                    pp.set_alpha(0.75)

            ax.set_title(label_title)
            fig.canvas.manager.set_window_title(label_title)
            ax.set_xlabel("Temperature difference (model - reference)")
            ax.set_ylabel("Frequency")
            ax.xaxis.set_major_formatter(FormatStrFormatter("%.0f°C"))
            ax.yaxis.set_major_formatter(PercentFormatter(xmax=1, decimals=0))
            ax.grid()
            ax.text(0.005, 0.95, self.id, transform=ax.transAxes, color='gray', alpha=0.75, ha='left', va='bottom')
            plt.legend(bbox_to_anchor =(0.5,-0.3), loc='lower center', ncol=2, facecolor='white', framealpha=0.8)
            fig.tight_layout()
            if not path_out is None:
                fig.savefig(os.path.join(path_out, "%s - error comparison.png"%label_title), bbox_inches='tight')

            # Cumulative distribution
            fig = plt.figure(dpi=DPI)
            ax = fig.add_subplot(1, 1, 1)
            lst_p = []
            for k, c in enumerate(df_error.columns):
                values, bins, p = ax.hist(df_error[c], bins=bins, weights=weights[:,k], cumulative=True, label=c)
                area_tmp = sum(np.diff(bins)*values)
                lst_p.append({"p": p, "area": area_tmp})
            lst_p = sorted(lst_p, key=lambda d: d['area'], reverse=True)
            for k, d in enumerate(lst_p):
                for pp in d["p"]:
                    pp.set_zorder(k+1)
                    pp.set_alpha(0.75)

            ax.set_title(label_title)
            fig.canvas.manager.set_window_title(label_title)
            ax.set_xlabel("Temperature difference (model - reference)")
            ax.set_ylabel("Frequency")
            ax.xaxis.set_major_formatter(FormatStrFormatter("%.0f°C"))
            ax.yaxis.set_major_formatter(PercentFormatter(xmax=1, decimals=0))
            ax.grid()
            ax.text(0.005, 0.95, self.id, transform=ax.transAxes, color='gray', alpha=0.75, ha='left', va='bottom')
            plt.legend(bbox_to_anchor =(0.5,-0.3), loc='lower center', ncol=2, facecolor='white', framealpha=0.8)
            fig.tight_layout()
            if not path_out is None:
                fig.savefig(os.path.join(path_out, "%s - cumulative error comparison.png"%label_title), bbox_inches='tight')

        if "individual_graph" in analysisToPerform:
            ## Error distribution
            for name in df_error.columns:
                df_error_one = df_error[name]

                fig = plt.figure(dpi=DPI)
                ax = fig.add_subplot(1, 1, 1)
                weights = np.ones_like(df_error_one) / df_error_one.size
                numbins = max(1, min(int((df_error_one.max() - df_error_one.min())/minbinwidth) + 1, maxbins))
                bins = np.linspace(df_error_one.min(), df_error_one.max(), num=numbins, endpoint=True)
                values, bins, p = ax.hist(df_error_one, bins=bins, weights=weights)
                fig.suptitle(label_title)
                ax.set_title(name)
                fig.canvas.manager.set_window_title("%s - %s"%(label_title, name))
                ax.set_xlabel("Temperature difference (model - reference)")
                ax.set_ylabel("Frequency")
                ax.xaxis.set_major_formatter(FormatStrFormatter("%.0f°C"))
                ax.yaxis.set_major_formatter(PercentFormatter(xmax=1, decimals=0))
                ax.grid()
                ax.text(0.005, 0.95, self.id, transform=ax.transAxes, color='gray', alpha=0.75, ha='left', va='bottom')
                plt.legend(bbox_to_anchor =(0.5,-0.3), loc='lower center', ncol=2, facecolor='white', framealpha=0.8)
                fig.tight_layout()
                if not path_out is None:
                    fig.savefig(os.path.join(path_out, "%s - %s - error.png"%(label_title, name)), bbox_inches='tight')

                ## Cumulative error
                fig = plt.figure(dpi=DPI)
                ax = fig.add_subplot(1, 1, 1)
                values, bins, p = ax.hist(df_error_one, bins=bins, weights=weights, cumulative=True)

                # Calculate percentiles
                quant_5, quant_25, quant_50, quant_75, quant_95 = df_error_one.quantile(0.05), df_error_one.quantile(0.25), df_error_one.quantile(0.5), df_error_one.quantile(0.75), df_error_one.quantile(0.95)
                quants = [[quant_5, 0.05], [quant_25, 0.25], [quant_50, 0.50],  [quant_75, 0.75], [quant_95, 0.95]]
                # Plot the lines with a loop
                for i in quants:
                    ax.axvline(i[0], ymax = i[1], linestyle = ":", color = "k")

                # Annotations
                ax.text(quant_5-1.0, 0.10, "5th: %.0f°C"%quant_5, size = 12).set_bbox(dict(facecolor='white', alpha=0.8, linewidth=0))
                ax.text(quant_25-1.0, 0.30, "25th: %.0f°C"%quant_25, size = 12).set_bbox(dict(facecolor='white', alpha=0.8, linewidth=0))
                ax.text(quant_50-1.0, 0.55, "50th: %.0f°C"%quant_50, size = 12).set_bbox(dict(facecolor='white', alpha=0.8, linewidth=0))
                ax.text(quant_75-1.0, 0.80, "75th: %.0f°C"%quant_75, size = 12).set_bbox(dict(facecolor='white', alpha=0.8, linewidth=0))
                ax.text(quant_95-1.0, 1.0, "95th: %.0f°C"%quant_95, size = 12).set_bbox(dict(facecolor='white', alpha=0.8, linewidth=0))

                fig.suptitle(label_title)
                ax.set_title(name)
                fig.canvas.manager.set_window_title("%s - %s"%(label_title, name))
                ax.set_xlabel("Temperature difference (model - reference)")
                ax.set_ylabel("Frequency")
                ax.xaxis.set_major_formatter(FormatStrFormatter("%.0f°C"))
                ax.yaxis.set_major_formatter(PercentFormatter(xmax=1, decimals=0))
                ax.grid()
                ax.text(0.005, 0.95, self.id, transform=ax.transAxes, color='gray', alpha=0.75, ha='left', va='bottom')
                plt.legend(bbox_to_anchor =(0.5,-0.3), loc='lower center', ncol=2, facecolor='white', framealpha=0.8)
                fig.tight_layout()
                if not path_out is None:
                    fig.savefig(os.path.join(path_out, "%s - %s - cumulative error.png"%(label_title, name)), bbox_inches='tight')

                ## Error vs. load factor curve
                fig = plt.figure(dpi=DPI)
                ax = fig.add_subplot(1, 1, 1)
                ax.scatter(self.ts_resampled.load.loc[self.ts_resampled.time.isin(df_tocompare.time)], df_error_one)
                fig.suptitle(label_title)
                ax.set_title(name)
                fig.canvas.manager.set_window_title("%s - %s"%(label_title, name))
                ax.set_xlabel("Load factor")
                ax.set_ylabel("Temperature difference (model - reference)")
                ax.xaxis.set_major_formatter(PercentFormatter(xmax=1, decimals=0))
                ax.yaxis.set_major_formatter(FormatStrFormatter("%.0f°C"))
                ax.grid()
                ax.text(0.005, 0.95, self.id, transform=ax.transAxes, color='gray', alpha=0.75, ha='left', va='bottom')
                plt.legend(bbox_to_anchor =(0.5,-0.3), loc='lower center', ncol=2, facecolor='white', framealpha=0.8)
                fig.tight_layout()
                if not path_out is None:
                    fig.savefig(os.path.join(path_out, "%s - %s - error vs load.png"%(label_title, name)), bbox_inches='tight')

                ## Error vs. ambient temperature curve
                fig = plt.figure(dpi=DPI)
                ax = fig.add_subplot(1, 1, 1)
                ax.scatter(self.ts_resampled.t_amb.loc[self.ts_resampled.time.isin(df_tocompare.time)], df_error_one)
                fig.suptitle(label_title)
                ax.set_title(name)
                fig.canvas.manager.set_window_title("%s - %s"%(label_title, name))
                ax.set_xlabel("Ambient temperature")
                ax.set_ylabel("Temperature difference (model - reference)")
                ax.xaxis.set_major_formatter(FormatStrFormatter("%.0f°C"))
                ax.yaxis.set_major_formatter(FormatStrFormatter("%.0f°C"))
                ax.grid()
                ax.text(0.005, 0.95, self.id, transform=ax.transAxes, color='gray', alpha=0.75, ha='left', va='bottom')
                plt.legend(bbox_to_anchor =(0.5,-0.3), loc='lower center', ncol=2, facecolor='white', framealpha=0.8)
                fig.tight_layout()
                if not path_out is None:
                    fig.savefig(os.path.join(path_out, "%s - %s - error vs t_amb.png"%(label_title, name)), bbox_inches='tight')

    def metricsEvaluation(self, path_metrics, evaluate_filtered_metrics=True, **kwargs):
        """
        Evaluate the TTS metrics
        """
        # List of timeseries to analyse
        lst_ts = []
        for name in TIMESERIES_NAME:
            if name in self.ts_resampled.columns:
                lst_ts.append(name)
        ts_eval = self.ts_resampled[lst_ts]

        # Path file for TTS metrics
        writer = pd.ExcelWriter(path_metrics, engine='xlsxwriter')
        def addMetrics(ts, sheet_name, count_full=None):
            ts_tmp = ts.drop(["time"], axis=1)

            ## For all the timeseries
            s_metrics = ts_tmp.describe(percentiles=[0.01, 0.05, 0.95, 0.99])
            #s_metrics.drop("count", inplace = True)

            # Range of the profile
            rng = ts_tmp.max() - ts_tmp.min()

            # Average absolute rate of change
            roc = (ts_tmp.diff().div(ts.time.diff().dt.total_seconds(), axis=0)).abs().mean()*3600

            s_metrics = pd.concat((s_metrics, pd.DataFrame({'RNG': rng,
                                                            'ROC (/h)': roc}).transpose()), axis=0)
            s_metrics.to_excel(writer, sheet_name=sheet_name, float_format = "%.2f")

            ## Specific calculation
            # Loading Dynamic Metric
            if not count_full is None:
                ldm = ts.time.count()/count_full*100

                df_tmp = pd.DataFrame({'LDM (%)': [ldm]}).transpose()
                df_tmp.to_excel(writer, sheet_name=sheet_name, startrow=s_metrics.shape[0]+2, header=False, float_format = "%.2f")

        ### Percentiles
        addMetrics(ts_eval, 'full')
        count_full = ts_eval.time.count()

        dict_filters = {}

        ### Percentiles on load
        if "load" in ts_eval.columns and evaluate_filtered_metrics:
            tmp = self.getFilteredTimeseries(ts_eval, "on-load", **kwargs)
            addMetrics(tmp, 'on-load', count_full)
            dict_filters['on-load'] = tmp.time

        ### TRF and SSRF
        if "t_tl" in ts_eval.columns and evaluate_filtered_metrics:
            tmp = self.getFilteredTimeseries(ts_eval, "TRF", **kwargs)
            addMetrics(tmp, 'TRF', count_full)
            dict_filters['TRF'] = tmp.time
            tmp = self.getFilteredTimeseries(ts_eval, "SSRF", **kwargs)
            addMetrics(tmp, 'SSRF', count_full)
            dict_filters['SSRF'] = tmp.time

        # Final
        writer.close()
        print("TTS metrics written in %s"%path_metrics)

        return dict_filters

    def getFilteredTimeseries(self, ts_eval, filter_type, **kwargs):
        """
        Get filtered resampled timeseries on different criteria
        ts_eval: timeserie to apply the filter
        filter_type:
        - "on-load": filter for periods when the transformer is on-load.
            - threshold: threshold in pu to consider the transformer on-load, 0.01 if not specified
        - "TRF": Transient Response Filter
            - threshold: threshold in K to consider, 3%*T_tl_iec (of the model IEC60076_top_liquid) if not specified
            - dttm_name: DTTM to use for the evaluation (must have transient and steady state model), IEC60076_VCS1_top_liquid if not specified
        - "SSRF": Steady State Response Filter
            - threshold: threshold in K to consider, 3%*T_tl_iec (of the model IEC60076_top_liquid) if not specified
            - dttm_name: DTTM to use for the evaluation (must have transient and steady state model), IEC60076_VCS1_top_liquid if not specified
        """
        if filter_type == "on-load":
            ### On-load periods
            if "threshold" in kwargs.keys():
                threshold = kwargs["threshold"]
            else:
                threshold = 0.01

            if "load" in ts_eval.columns:
                return ts_eval.loc[ts_eval.load>threshold]
            raise Exception("No timeserie load")

        elif filter_type in ["TRF", "SSRF"]:
            ### Loading Dynamic Metric and Loading Factor Metric
            ## Get parameters
            # Model to consider for transient and steady-state evaluation
            if "dttm" in kwargs.keys():
                dttm_name = kwargs["dttm"]
            else:
                dttm_name = "IEC60076_VCS1_top_liquid"
            if "threshold" in kwargs.keys():
                threshold = kwargs["threshold"]
            else:
                if filter_type == "TRF":
                    threshold = 0.05*self.params.IEC60076_top_liquid.T_tl_iec.value
                else: # SSRF
                    threshold = 0.01*self.params.IEC60076_top_liquid.T_tl_iec.value

            ## Get DTTM class
            lst_py_files = [f[:-3] for f in os.listdir(os.path.join(".", "dttm")) if f.endswith('.py') and f != '__init__.py' and f != 'DTTM.py']
            dttm = None
            for py in lst_py_files:
                mod = __import__('.'.join(["dttm", py]), fromlist=[py])
                classes = [getattr(mod, x) for x in dir(mod) if isinstance(getattr(mod, x), type) and x != 'DTTM' and x != 'DTTMProprietary']
                for cl in classes:
                    tmp = cl()
                    if tmp.shortname == dttm_name:
                        dttm = tmp
                        break
            if dttm is None:
                raise Exception("Couldn't find DTTM %s"%dttm_name)
            # Check if DTTM has steady state model
            if not hasattr(dttm, "evaluateSteadyStateModel"):
                raise Exception("DTTM %s doesn't have a steady state model implemented"%dttm_name)

            ## Evaluate DTTM transient and steady mode
            if dttm.type == "top_liquid":
                ts_result_transient = dttm.evaluateModel(self, initial_value=self.ts_resampled.t_tl.iloc[0])
                ts_result_steady = dttm.evaluateSteadyStateModel(self, initial_value=self.ts_resampled.t_tl.iloc[0])
                key_ts = "t_tl_model"
            elif dttm.type == "hotspot":
                ts_result_transient = dttm.evaluateModel(self, initial_value=self.ts_resampled.t_hs.iloc[0])
                ts_result_steady = dttm.evaluateSteadyStateModel(self, initial_value=self.ts_resampled.t_hs.iloc[0])
                key_ts = "t_hs_model"
            else:
                raise Exception("DTTM type not implemented: %s"%dttm.type)

            ## Evaluate filter
            diff_transient_steady = (ts_result_transient[key_ts] - ts_result_steady[key_ts]).abs()
            if filter_type == "TRF":
                return ts_eval.loc[diff_transient_steady>=threshold]
            else: # SSRF
                return ts_eval.loc[diff_transient_steady<=threshold]

        else:
            raise Exception("Unknown filter type: %s"%filter_type)









