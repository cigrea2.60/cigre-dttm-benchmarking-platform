# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import datetime as dt
import dateutil.parser
import copy, time, os
from openpyxl import Workbook, load_workbook

from matplotlib.ticker import PercentFormatter
import matplotlib.pyplot as plt
plt.close("all")

# =============================================================================
# DTTM-BP
# =============================================================================

### Paths definition
path_tts = "./tts"


### TTS to load
id_tfo = "a"
id_input_ds = "01"

# Timestep to use
timeStep = 10*60 #s


# =============================================================================
# Load Timeseries as a DataFrame
# =============================================================================
path_file = os.path.join(path_tts, "TTS%s"%id_tfo, "TTS%s%s-timeseries_1sheet.xlsx"%(id_tfo, id_input_ds))
path_file = os.path.join(path_tts, "TTS%s"%id_tfo, "TTS%s%s-timeseries - Copie.xlsx"%(id_tfo, id_input_ds))
path_file = os.path.join(path_tts, "TTS%s"%id_tfo, "TTS%s%s-timeseries.xlsx"%(id_tfo, id_input_ds))
df_dict = pd.read_excel(path_file, index_col=None, sheet_name=None)

# Merge sheets base on time, if no value available then it's NaN value
for k, sheet_name in enumerate(df_dict.keys()):
    print("Load %s"%sheet_name)
    df_tmp = df_dict[sheet_name]

    # Remove unnamed columns
    df_tmp.drop(df_tmp.columns[df_tmp.columns.str.contains('unnamed', case=False)], axis = 1, inplace = True)

    # Merge in one dataframe
    if k == 0:
        tts_ts = df_tmp.copy()
    else:
        tts_ts = pd.merge_ordered(tts_ts, df_tmp, fill_method='None', on="time")



### Resample timeserie
# New time array
ts = np.arange(tts_ts.time.min().to_pydatetime(), tts_ts.time.max().to_pydatetime(), timeStep*1000000)
df_ts_new = pd.DataFrame({'time': ts})

# Add new timestamps
df_tmp = pd.merge_ordered(df_ts_new, tts_ts, fill_method='None', on="time")

# Linear interpolation based on timestamps
df_tmp.set_index('time', inplace=True)
df_tmp.interpolate(method='index', inplace=True)
df_tmp.reset_index(inplace=True)
df_tmp.fillna(method="bfill", inplace=True)

# Extraction at the requested timestamps
tts_ts = df_tmp[df_tmp.time.isin(df_ts_new.time)].copy()

# # =============================================================================
# # Load Parameters as a Dict
# # =============================================================================
# path_file = os.path.join(path_tts, "TTS%s"%id_tfo, "TTS%s-parameters.xlsx"%id_tfo)

# # Import Workbook
# wb = load_workbook(path_file, data_only=True)

# # Load Worksheet
# if len(wb.sheetnames) > 1:
#     print("Warning! Only worksheet %s will be loaded."%wb.sheetnames[0])
# ws = wb[wb.sheetnames[0]]

# # Data columns and rows
# k_line_start_data = -1
# k_column_longname = -1
# k_column_unit = -1
# k_column_shortname = -1
# k_column_value = -1
# for row in ws.iter_rows():
#     for cell in row:
#         if cell.value == "Parameters longname":
#             k_column_longname = cell.column
#             k_line_start_data = cell.row + 1 # Data begining
#         elif cell.value == "unit":
#             k_column_unit = cell.column
#         elif cell.value == "shortname":
#             k_column_shortname = cell.column
#         elif cell.value == "value":
#             k_column_value = cell.column

# if k_line_start_data < 0 or k_column_longname < 0 or k_column_unit < 0 or k_column_shortname < 0 or k_column_value < 0:
#     raise Exception("Problem with file %s data"%path_file)

# # Load data
# class dotdict(dict):
#     """dot.notation access to dictionary attributes"""
#     __getattr__ = dict.get
#     __setattr__ = dict.__setitem__
#     __delattr__ = dict.__delitem__

# k_column_min = min(k_column_longname, k_column_unit, k_column_shortname, k_column_value)
# k_column_max = max(k_column_longname, k_column_unit, k_column_shortname, k_column_value)
# k_column_longname_bis = k_column_longname - k_column_min
# k_column_unit_bis = k_column_unit - k_column_min
# k_column_shortname_bis = k_column_shortname - k_column_min
# k_column_value_bis = k_column_value - k_column_min

# tts_params = {}
# for row in ws.iter_rows(k_line_start_data, None, k_column_min, k_column_max, False):

#     if not row[k_column_shortname_bis].value is None:

#         name = row[k_column_shortname_bis].value
#         tts_params[name] = {}
#         tts_params[name]["longname"] = row[k_column_longname_bis].value
#         tts_params[name]["unit"] = row[k_column_unit_bis].value
#         tts_params[name]["value"] = row[k_column_value_bis].value
#         tts_params[name] = dotdict(tts_params[name])

# tts_params = dotdict(tts_params)

# # =============================================================================
# # Create TTS object
# # =============================================================================






