# -*- coding: utf-8 -*-

from .DTTMProprietary import DTTMProprietary

"""
To add a Proprietary DTTM,
    1. Copy/paste the example below at the end of the file
    2. Change the class name 'ExampleProprietaryModel'
    3. Change the longname/type_dttm according to the Proprietary DTTM to include
"""

# #################### Begin of the example ####################
# class ExampleProprietaryModel(DTTMProprietary):
#     """
#     Example of a proprietary model
#     """

#     def __init__(self):
#         """
#         Class constructor
#         """
#         super().__init__(longname = "Example of Proprietary DTTM",
#                          description = "Example.",
#                          type_dttm = "top_liquid",
#                          required_ts = ['t_amb', 'load', 'cooling_stage'])
# #################### End of the example ####################

class EDF_ODAF_top_liquid(DTTMProprietary):
    """
    EDF ODAF top liquid model
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "EDF ODAF top liquid model",
                         description = "Analytical (lumped element) model",
                         type_dttm = "top_liquid",
                         required_ts = ['t_amb', 'load', 'cooling_stage'])

class EDF_ODAF_hotspot(DTTMProprietary):
    """
    EDF ODAF hotspot model
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "EDF ODAF hotspot model",
                         description = "Analytical (lumped element) model",
                         type_dttm = "hotspot",
                         required_ts = ['t_tl', 'load', 'cooling_stage'])

class LoadGT_IEC_top_liquid(DTTMProprietary):
    """
    LoadGT IEC (Portillo) top liquid model
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "LoadGT-IEC (Portillo) top liquid model",
                         description = "Analytical (lumped element) model",
                         type_dttm = "top_liquid",
                         required_ts = ['t_amb', 'load'])

class LoadGT_IEEE_top_liquid(DTTMProprietary):
    """
    LoadGT IEEE (Portillo) top liquid model
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "LoadGT-IEEE (Portillo) top liquid model",
                         description = "Analytical (lumped element) model",
                         type_dttm = "top_liquid",
                         required_ts = ['t_amb', 'load'])

class LoadGT_IEC_hotspot(DTTMProprietary):
    """
    LoadGT IEC (Portillo) hotspot model
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "LoadGT-IEC (Portillo) hotspot model",
                         description = "Analytical (lumped element) model",
                         type_dttm = "hotspot",
                         required_ts = ['t_tl', 'load'])

class KTH_ML_top_liquid(DTTMProprietary):
    """
    Machine Learning evaluation at KTH top liquid model
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "Machine Learning evaluation at KTH top liquid model",
                         description = "Machine learned (neural network) model",
                         type_dttm = "top_liquid",
                         required_ts = ['t_amb', 'load'])

class KTH_ML_hotspot(DTTMProprietary):
    """
    Machine Learning evaluation at KTH hotspot model
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "Machine Learning evaluation at KTH hotspot model",
                         description = "Machine learned (neural network) model",
                         type_dttm = "hotspot",
                         required_ts = ['t_tl', 'load'])

class THAM_top_liquid(DTTMProprietary):
    """
    THAM top liquid model
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "THAM top liquid model",
                         description = "THN model",
                         type_dttm = "top_liquid",
                         required_ts = ['t_amb', 'load'])

class THAM_hotspot(DTTMProprietary):
    """
    THAM hotspot model
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "THAM hotspot model",
                         description = "THN model",
                         type_dttm = "hotspot",
                         required_ts = ['t_tl', 'load'])

