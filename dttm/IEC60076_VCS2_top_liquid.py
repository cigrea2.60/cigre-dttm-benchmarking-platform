# -*- coding: utf-8 -*-

import numpy as np

from .DTTM import DTTM

class IEC60076_VCS2_top_liquid(DTTM):
    """
    IEC 60076-7 (2017) Top liquid model, with Variable Cooling Stage improvement
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "IEC 60076-7 VCS 2 top liquid model",
                         description = "IEC 60076-7 top liquid model with modification \
                         to take into account the cooling stage (rated power for each cooling stage)",
                         type_dttm = "top_liquid",
                         required_params=['k11', 'S_nom_0_iec', 'S_nom_1_iec', 'S_nom', 'R', 'x_iec', 'T_tl_iec'],
                         required_ts=['t_amb', 'load', 'cooling_stage'])

    def evaluateModel(self, tts, **kwargs):
        """
        Evaluate model on TTS with the initial value
        """

        # Call mother method for initialization
        ts_result = super().evaluateModel(tts, **kwargs)
        i_first = ts_result.t_tl_model.isna().idxmax()

        params_model = tts.params[self.shortname]

        deltaT = ts_result.time.diff().dt.total_seconds() #s

        for i in range(i_first, len(ts_result)):

            # Time constant and top-liquid temperature rise depend on the cooling stage
            if ts_result.cooling_stage.iloc[i] == 0:
                T_tl = np.power((1 + np.power(params_model.S_nom_0_iec.value/params_model.S_nom.value, 2)*params_model.R.value)/(1 + params_model.R.value), - params_model.x_iec.value)*params_model.T_tl_iec.value
            elif ts_result.cooling_stage.iloc[i] == 1:
                T_tl = np.power((1 + np.power(params_model.S_nom_1_iec.value/params_model.S_nom.value, 2)*params_model.R.value)/(1 + params_model.R.value), - params_model.x_iec.value)*params_model.T_tl_iec.value
            elif ts_result.cooling_stage.iloc[i] == 2:
                T_tl = params_model.T_tl_iec.value
            else:
                raise Exception("Unknown cooling stage !: %s"%ts_result.cooling_stage.iloc[i])

            # Dθo (18) §8.2.3
            DTho = deltaT.iloc[i]/(params_model.k11.value*params_model.Tau_tl_iec.value*60.0)*(np.power((1 + np.power(ts_result.load.iloc[i], 2)*params_model.R.value)/(1 + params_model.R.value), params_model.x_iec.value)*T_tl - (ts_result.t_tl_model.iloc[i-1] - ts_result.t_amb.iloc[i]))
            # (19) §8.2.3
            ts_result.loc[i, 't_tl_model'] = ts_result.t_tl_model.iloc[i-1] + DTho

        return ts_result

    def evaluateSteadyStateModel(self, tts, **kwargs):
        """
        Evaluate steady state model on TTS with the initial value
        """

        # Call mother method for initialization
        ts_result = super().evaluateModel(tts, **kwargs)
        i_first = ts_result.t_tl_model.isna().idxmax()

        params_model = tts.params[self.shortname]

        for i in range(i_first, len(ts_result)):

            # Time constant and top-liquid temperature rise depend on the cooling stage
            if ts_result.cooling_stage.iloc[i] == 0:
                T_tl = np.power((1 + np.power(params_model.S_nom_0_iec.value/params_model.S_nom.value, 2)*params_model.R.value)/(1 + params_model.R.value), - params_model.x_iec.value)*params_model.T_tl_iec.value
            elif ts_result.cooling_stage.iloc[i] == 1:
                T_tl = np.power((1 + np.power(params_model.S_nom_1_iec.value/params_model.S_nom.value, 2)*params_model.R.value)/(1 + params_model.R.value), - params_model.x_iec.value)*params_model.T_tl_iec.value
            elif ts_result.cooling_stage.iloc[i] == 2:
                T_tl = params_model.T_tl_iec.value
            else:
                raise Exception("Unknown cooling stage !: %s"%ts_result.cooling_stage.iloc[i])

            # Steady-state model is calculated considering dTheta/dt=0 in (5) §8.2.1
            ts_result.loc[i, 't_tl_model'] = ts_result.t_amb.iloc[i] + np.power((1 + np.power(ts_result.load.iloc[i], 2)*params_model.R.value)/(1 + params_model.R.value), params_model.x_iec.value)*T_tl

        return ts_result


