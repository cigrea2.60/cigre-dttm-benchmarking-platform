# -*- coding: utf-8 -*-

import numpy as np

from .DTTM import DTTM

class IEC60076_VTP1_hotspot(DTTM):
    """
    IEC 60076-7 (2017) hotspot model, with Variable Tap Position improvement
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "IEC 60076-7 VTP 1 hotspot model",
                         description = "IEC 60076-7 hotspot model with modification \
                         to take into account the variable tap position (oil time constant and rated top oil temperature rise \
                         depend on the tap, IEC 60076-2:2011 approach). Refer to CIGRE 2023 Split article 'Dynamic thermal modelling of power transformer \
                         with variable tap position' for details.",
                         type_dttm = "hotspot",
                         required_params=['k21', 'k22', 'y_iec', 'x_iec_76_2', 'y_iec_76_2', 'z_iec_76_2', \
                                          'Tau_hgr_iec', 'Tau_tl_iec', 'T_tl_iec', \
                                          'tap_pos_r', 'tap_pos_r', 'tap_pos_minus', 'tap_pos_plus', \
                                          'g_w', 'g_hs', 'loc_hs_tap', 'R', 'R_minus', 'R_plus', 'R_rp1', 'R_trt'],
                         required_ts=['t_tl', 'load', 'tap_pos'])

    def evaluateModel(self, tts, **kwargs):
        """
        Evaluate model on TTS with the initial value
        """

        # Call mother method for initialization
        ts_result = super().evaluateModel(tts, **kwargs)
        i_first = ts_result.t_hs_model.isna().idxmax()

        # Others initialization
        dTh1 = np.empty(len(ts_result.time))
        dTh1[:] = np.nan
        dTh2 = dTh1.copy()
        dTh = dTh1.copy()
        dTh[0:i_first] = ts_result.t_hs_model.iloc[0:i_first] - ts_result.t_tl.iloc[0:i_first]
        dTh1[0:i_first] = dTh[0:i_first]
        dTh2[0:i_first] = 0

        params_model = tts.params[self.shortname]

        deltaT = ts_result.time.diff().dt.total_seconds() #s

        for i in range(i_first, len(ts_result)):

            # Dependance of the losses on the tap position
            if ts_result.tap_pos.iloc[i] <= params_model.tap_pos_r.value:
                # IEC 60076-7:2018-01 §9.2
                m1 = (params_model.R.value - params_model.R_minus.value)/(params_model.tap_pos_r.value - params_model.tap_pos_minus.value)
                # IEC 60076-7:2018-01 §9.3 (30)
                R = params_model.R.value + (ts_result.tap_pos.iloc[i] - params_model.tap_pos_r.value)*m1
            else:
                # IEC 60076-7:2018-01 §9.2
                m2 = (params_model.R_plus.value - params_model.R_rp1.value)/(params_model.tap_pos_plus.value - (params_model.tap_pos_r.value + 1))
                # IEC 60076-7:2018-01 §9.3 (29)
                R = params_model.R_rp1.value + (ts_result.tap.iloc[i] - (params_model.tap_pos_r.value + 1))*m2

            # Dependance of the top-liquid temperature rise on the tap position, IEC 60076-2:2011-02 §7.13 + see article
            T_tl = params_model.T_tl_iec.value*np.power((1 + R)/(1 + params_model.R_trt.value), params_model.x_iec_76_2.value)

            if params_model.loc_hs_tap.value == "yes":
                # Current ratio between current tap and rated one
                ratio_current_HV = params_model["Ir_tap_%i"%ts_result.tap_pos.iloc[i]].value/params_model["Ir_tap_%i"%params_model.tap_pos_r.value].value
                # Average winding to liquid temperature gradient, IEC 60076-2:2011-02 §7.13 + see article
                g_w = params_model.g_w.value*np.power(ratio_current_HV, params_model.y_iec_76_2.value)
                # Hot-spot to top liquid temperature gradient, IEC 60076-2:2011-02 §7.13 + see article
                T_hgr = params_model.g_hs.value*np.power(ratio_current_HV, params_model.z_iec_76_2.value)
            else:
                g_w = params_model.g_w.value
                T_hgr = params_model.g_hs.value

            # # Dependance of the hot-spot winding temperature rise, IEC 60076-2:2011-02 §7.13 + see article
            # print(T_tl, params_model.T_tl_iec.value, g_hs, T_tl + g_hs)
            # T_hgr = T_tl + g_hs

            # Dependance of the top-liquid time constant on the tap position, from IEC 60076-7:2018-01 §E + see article
            Tau_tl = (T_tl/params_model.T_tl_iec.value)*((1 + params_model.R_trt.value)/(1 + R))*params_model.Tau_tl_iec.value

            if params_model.loc_hs_tap.value == "yes":
                # Dependance of the winding time constant on the tap position, from IEC 60076-7:2018-01 §E + see article
                Tau_hgr = params_model.Tau_hgr_iec.value*(g_w/params_model.g_w.value)*np.power(1/ratio_current_HV, 2)
            else:
                Tau_hgr = params_model.Tau_hgr_iec.value

            # Δθh1 (20) §8.2.3
            DdTh1 = deltaT.iloc[i]/(params_model.k22.value*Tau_hgr*60.0)*(params_model.k21.value*T_hgr*np.power(ts_result.load.iloc[i], params_model.y_iec.value) - dTh1[i-1])
            dTh1[i] = dTh1[i-1] + DdTh1
            # Δθh2 (21) §8.2.3
            DdTh2 = deltaT.iloc[i]/((1/params_model.k22.value)*Tau_tl*60.0)*((params_model.k21.value - 1)*T_hgr*np.power(ts_result.load.iloc[i], params_model.y_iec.value) - dTh2[i-1])
            dTh2[i] = dTh2[i-1] + DdTh2
            # Δθh (22) §8.2.3
            dTh[i] = dTh1[i] + dTh2[i]
            # (23) §8.2.3
            ts_result.loc[i, 't_hs_model'] = ts_result.t_tl.iloc[i] + dTh[i]

        return ts_result

    def evaluateSteadyStateModel(self, tts, **kwargs):
        """
        Evaluate steady state model on TTS with the initial value
        """

        # Call mother method for initialization
        ts_result = super().evaluateModel(tts, **kwargs)
        i_first = ts_result.t_hs_model.isna().idxmax()

        params_model = tts.params[self.shortname]

        for i in range(i_first, len(ts_result)):

            if params_model.loc_hs_tap.value == "yes":
                # Current ratio between current tap and rated one
                ratio_current_HV = params_model["Ir_tap_%i"%ts_result.tap_pos.iloc[i]].value/params_model["Ir_tap_%i"%params_model.tap_pos_r.value].value
                # Hot-spot to top liquid temperature gradient, IEC 60076-2:2011-02 §7.13 + see article
                T_hgr = params_model.g_hs.value*np.power(ratio_current_HV, params_model.z_iec_76_2.value)
            else:
                T_hgr = params_model.g_hs.value

            # Steady-state model is calculated considering dTheta/dt=0 in (6,7,8,9) §8.2.1
            ts_result.loc[i, 't_hs_model'] = ts_result.t_tl.iloc[i] + T_hgr*np.power(ts_result.load.iloc[i], params_model.y_iec.value)

        return ts_result

