# -*- coding: utf-8 -*-

import numpy as np

from .DTTM import DTTM

class IEC60076_VCS1amb_hotspot(DTTM):
    """
    IEC 60076-7 (2017) hotspot model from the ambient temperature, with Variable Cooling Stage improvement
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "IEC 60076-7 VCS1amb hotspot model",
                         description = "IEC 60076-7 hotspot model with modification \
                         to take into account the cooling stage (oil time constant \
                         depends on the cooling stage), from the ambient temperature",
                         type_dttm = "hotspot",
                         required_params=['k11', 'Tau_tl_0_iec', 'Tau_tl_1_iec', 'Tau_tl_2_iec', 'R', 'x_iec', 'T_tl_0_iec', 'T_tl_1_iec', 'T_tl_2_iec', \
                                          'k21', 'k22', 'Tau_hgr_iec', 'T_hgr_iec', 'y_iec'],
                         required_ts=['t_amb', 'load', 'cooling_stage'])

    def evaluateModel(self, tts, **kwargs):
        """
        Evaluate model on TTS with the initial value
        """

        # Call mother method for initialization
        ts_result = super().evaluateModel(tts, **kwargs)
        i_first = ts_result.t_hs_model.isna().idxmax()

        # Others initialization
        t_tl_model = np.empty(len(ts_result.time))
        t_tl_model[:] = np.nan
        t_tl_model[0:i_first] = ts_result.t_amb.iloc[0:i_first]

        dTh1 = np.empty(len(ts_result.time))
        dTh1[:] = np.nan
        dTh2 = dTh1.copy()
        dTh = dTh1.copy()
        dTh[0:i_first] = ts_result.t_hs_model.iloc[0:i_first] - t_tl_model[0:i_first]
        dTh1[0:i_first] = dTh[0:i_first]
        dTh2[0:i_first] = 0

        params_model = tts.params[self.shortname]

        deltaT = ts_result.time.diff().dt.total_seconds() #s

        for i in range(i_first, len(ts_result)):

            # Top-liquid time constant depend on the cooling stage
            if ts_result.cooling_stage.iloc[i] == 0:
                T_tl = params_model.T_tl_0_iec.value
                Tau_tl = params_model.Tau_tl_0_iec.value
            elif ts_result.cooling_stage.iloc[i] == 1:
                T_tl = params_model.T_tl_1_iec.value
                Tau_tl = params_model.Tau_tl_1_iec.value
            elif ts_result.cooling_stage.iloc[i] == 2:
                T_tl = params_model.T_tl_2_iec.value
                Tau_tl = params_model.Tau_tl_2_iec.value
            else:
                raise Exception("Unknown cooling stage !: %s"%ts_result.cooling_stage.iloc[i])

            ## Top-liquid
            # Dθo (18) §8.2.3
            DTho = deltaT.iloc[i]/(params_model.k11.value*Tau_tl*60.0)*(np.power((1 + np.power(ts_result.load.iloc[i], 2)*params_model.R.value)/(1 + params_model.R.value), params_model.x_iec.value)*T_tl - (t_tl_model[i-1] - ts_result.t_amb.iloc[i]))
            # (19) §8.2.3
            t_tl_model[i] = t_tl_model[i-1] + DTho

            ## Hot-spot
            # Δθh1 (20) §8.2.3
            DdTh1 = deltaT.iloc[i]/(params_model.k22.value*params_model.Tau_hgr_iec.value*60.0)*(params_model.k21.value*params_model.T_hgr_iec.value*np.power(ts_result.load.iloc[i], params_model.y_iec.value) - dTh1[i-1])
            dTh1[i] = dTh1[i-1] + DdTh1
            # Δθh2 (21) §8.2.3
            DdTh2 = deltaT.iloc[i]/((1/params_model.k22.value)*Tau_tl*60.0)*((params_model.k21.value - 1)*params_model.T_hgr_iec.value*np.power(ts_result.load.iloc[i], params_model.y_iec.value) - dTh2[i-1])
            dTh2[i] = dTh2[i-1] + DdTh2
            # Δθh (22) §8.2.3
            dTh[i] = dTh1[i] + dTh2[i]
            # (23) §8.2.3
            ts_result.loc[i, 't_hs_model'] = t_tl_model[i] + dTh[i]

        return ts_result

    def evaluateSteadyStateModel(self, tts, **kwargs):
        """
        Evaluate steady state model on TTS with the initial value
        """

        # Call mother method for initialization
        ts_result = super().evaluateModel(tts, **kwargs)
        i_first = ts_result.t_hs_model.isna().idxmax()

        params_model = tts.params[self.shortname]

        for i in range(i_first, len(ts_result)):

            # Time constant and top-liquid temperature rise depend on the cooling stage
            if ts_result.cooling_stage.iloc[i] == 0:
                T_tl = params_model.T_tl_0_iec.value
            elif ts_result.cooling_stage.iloc[i] == 1:
                T_tl = params_model.T_tl_1_iec.value
            elif ts_result.cooling_stage.iloc[i] == 2:
                T_tl = params_model.T_tl_2_iec.value
            else:
                raise Exception("Unknown cooling stage !: %s"%ts_result.cooling_stage.iloc[i])

            # Steady-state model is calculated considering dTheta/dt=0 in (5) §8.2.1
            t_tl_model = ts_result.t_amb.iloc[i] + np.power((1 + np.power(ts_result.load.iloc[i], 2)*params_model.R.value)/(1 + params_model.R.value), params_model.x_iec.value)*T_tl

            # Steady-state model is calculated considering dTheta/dt=0 in (6,7,8,9) §8.2.1
            ts_result.loc[i, 't_hs_model'] = t_tl_model + params_model.T_hgr_iec.value*np.power(ts_result.load.iloc[i], params_model.y_iec.value)

        return ts_result


