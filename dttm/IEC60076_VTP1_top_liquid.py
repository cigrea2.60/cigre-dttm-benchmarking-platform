# -*- coding: utf-8 -*-

import numpy as np

from .DTTM import DTTM

class IEC60076_VTP1_top_liquid(DTTM):
    """
    IEC 60076-7 (2017) Top liquid model, with Variable Tap Position improvement
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "IEC 60076-7 VTP 1 top liquid model",
                         description = "IEC 60076-7 top liquid model with modification \
                         to take into account the variable tap position (oil time constant and rated top oil temperature rise \
                         depend on the tap, IEC 60076-2:2011 approach). Refer to CIGRE 2023 Split article 'Dynamic thermal modelling of power transformer \
                         with variable tap position' for details.",
                         type_dttm = "top_liquid",
                         required_params=['k11', 'x_iec', 'x_iec_76_2', \
                                          'Tau_tl_iec', 'T_tl_iec', \
                                          'tap_pos_r', 'tap_pos_minus', 'tap_pos_plus', \
                                          'R', 'R_minus', 'R_plus', 'R_rp1', 'R_trt'],
                         required_ts=['t_amb', 'load', 'tap_pos'])

    def evaluateModel(self, tts, **kwargs):
        """
        Evaluate model on TTS with the initial value
        """

        # Call mother method for initialization
        ts_result = super().evaluateModel(tts, **kwargs)
        i_first = ts_result.t_tl_model.isna().idxmax()

        params_model = tts.params[self.shortname]

        deltaT = ts_result.time.diff().dt.total_seconds() #s

        for i in range(i_first, len(ts_result)):

            # Dependance of the losses on the tap position
            if ts_result.tap_pos.iloc[i] <= params_model.tap_pos_r.value:
                # IEC 60076-7:2018-01 §9.2
                m1 = (params_model.R.value - params_model.R_minus.value)/(params_model.tap_pos_r.value - params_model.tap_pos_minus.value)
                # IEC 60076-7:2018-01 §9.3 (30)
                R = params_model.R.value + (ts_result.tap_pos.iloc[i] - params_model.tap_pos_r.value)*m1
            else:
                # IEC 60076-7:2018-01 §9.2
                m2 = (params_model.R_plus.value - params_model.R_rp1.value)/(params_model.tap_pos_plus.value - (params_model.tap_pos_r.value + 1))
                # IEC 60076-7:2018-01 §9.3 (29)
                R = params_model.R_rp1.value + (ts_result.tap.iloc[i] - (params_model.tap_pos_r.value + 1))*m2

            # Dependance of the top-liquid temperature rise on the tap position, IEC 60076-2:2011-02 §7.13
            T_tl = params_model.T_tl_iec.value*np.power((1 + R)/(1 + params_model.R_trt.value), params_model.x_iec_76_2.value)

            # Dependance of the top-liquid time constant on the tap position, from IEC 60076-7:2018-01 §E + see article
            Tau_tl = (T_tl/params_model.T_tl_iec.value)*((1 + params_model.R_trt.value)/(1 + R))*params_model.Tau_tl_iec.value

            # IEC 60076-7:2018-01 Dθo (18) §8.2.3
            DTho = deltaT.iloc[i]/(params_model.k11.value*Tau_tl*60.0)*(np.power((1 + np.power(ts_result.load.iloc[i], 2)*R)/(1 + R), params_model.x_iec.value)*T_tl - (ts_result.t_tl_model.iloc[i-1] - ts_result.t_amb.iloc[i]))
            # IEC 60076-7:2018-01 (19) §8.2.3
            ts_result.loc[i, 't_tl_model'] = ts_result.t_tl_model.iloc[i-1] + DTho

        return ts_result

    def evaluateSteadyStateModel(self, tts, **kwargs):
        """
        Evaluate steady state model on TTS with the initial value
        """

        # Call mother method for initialization
        ts_result = super().evaluateModel(tts, **kwargs)
        i_first = ts_result.t_tl_model.isna().idxmax()

        params_model = tts.params[self.shortname]

        for i in range(i_first, len(ts_result)):

            # Dependance of the losses on the tap position
            if ts_result.tap_pos.iloc[i] <= params_model.tap_pos_r.value:
                # IEC 60076-7:2018-01 §9.2
                m1 = (params_model.R.value - params_model.R_minus.value)/(params_model.tap_pos_r.value - params_model.tap_pos_minus.value)
                # IEC 60076-7:2018-01 §9.3 (30)
                R = params_model.R.value + (ts_result.tap_pos.iloc[i] - params_model.tap_pos_r.value)*m1
            else:
                # IEC 60076-7:2018-01 §9.2
                m2 = (params_model.R_plus.value - params_model.R_rp1.value)/(params_model.tap_pos_plus.value - (params_model.tap_pos_r.value + 1))
                # IEC 60076-7:2018-01 §9.3 (29)
                R = params_model.R_rp1.value + (ts_result.tap.iloc[i] - (params_model.tap_pos_r.value + 1))*m2

            # Dependance of the top-liquid temperature rise on the tap position, IEC 60076-2:2011-02 §7.13
            T_tl = params_model.T_tl_iec.value*np.power((1 + R)/(1 + params_model.R_trt.value), params_model.x_iec_76_2.value)

            # Steady-state model is calculated considering dTheta/dt=0 in (5) §8.2.1
            ts_result.loc[i, 't_tl_model'] = ts_result.t_amb.iloc[i] + np.power((1 + np.power(ts_result.load.iloc[i], 2)*R)/(1 + R), params_model.x_iec.value)*T_tl

        return ts_result

