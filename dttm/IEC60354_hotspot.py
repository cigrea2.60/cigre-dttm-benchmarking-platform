# -*- coding: utf-8 -*-

import numpy as np

from .DTTM import DTTM

class IEC60354_hotspot(DTTM):
    """
    IEC 60354 (1991) hotspot model from bottom liquid temperature
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "IEC 60354 hotspot model",
                         description = "IEC 60354 (1991) hotspot model",
                         type_dttm = "hotspot",
                         required_params=['R', 'x_iec', 'y_iec', 'T_hgr_iec', 'Tau_bl_iec', 'T_ml_iec', 'T_bl_iec', 'Tau_w_iec'],
                         required_ts=['t_amb', 'load'])

    def evaluateModel(self, tts, **kwargs):
        """
        Evaluate model on TTS with the initial value
        """

        # Call mother method for initialization
        ts_result = super().evaluateModel(tts, **kwargs)
        i_first = ts_result.t_hs_model.isna().idxmax()

        # Others initialization
        t_bl_model = np.empty(len(ts_result.time))
        t_bl_model[:] = np.nan
        t_bl_model[0:i_first] = ts_result.t_amb.iloc[0:i_first]

        t_hs_bl_model = np.empty(len(ts_result.time))
        t_hs_bl_model[:] = np.nan
        t_hs_bl_model[0:i_first] = ts_result.t_hs_model.iloc[0:i_first] - t_bl_model[0:i_first]

        params_model = tts.params[self.shortname]

        deltaT = ts_result.time.diff().dt.total_seconds() #s

        for i in range(i_first, len(ts_result)):

            # Bottom liquid temperature
            t_bl_model[i] = t_bl_model[i-1] + (deltaT.iloc[i]/(params_model.Tau_bl_iec.value*60.0))*(np.power((1 + np.power(ts_result.load.iloc[i], 2)*params_model.R.value)/(1 + params_model.R.value), params_model.x_iec.value)*params_model.T_bl_iec.value + ts_result.t_amb.iloc[i] - t_bl_model[i-1])

            # Hot spot to bottom liquid gradient
            t_hs_bl_model[i] = t_hs_bl_model[i-1] + (deltaT.iloc[i]/(params_model.Tau_w_iec.value*60.0))*(2*(params_model.T_ml_iec.value - params_model.T_bl_iec.value)*np.power(ts_result.load.iloc[i], params_model.y_iec.value) + params_model.T_hgr_iec.value*np.power(ts_result.load.iloc[i], params_model.y_iec.value) + t_bl_model[i] - t_hs_bl_model[i-1])

            # Hot spot temperature
            ts_result.loc[i, 't_hs_model'] = t_bl_model[i] + t_hs_bl_model[i]

        return ts_result

    def evaluateSteadyStateModel(self, tts, **kwargs):
        """
        Evaluate steady state model on TTS with the initial value
        """

        # Call mother method for initialization
        ts_result = super().evaluateModel(tts, **kwargs)
        i_first = ts_result.t_hs_model.isna().idxmax()

        params_model = tts.params[self.shortname]

        for i in range(i_first, len(ts_result)):

            # Bottom liquid temperature
            t_bl_model = ts_result.t_amb.iloc[i] + np.power((1 + np.power(ts_result.load.iloc[i], 2)*params_model.R.value)/(1 + params_model.R.value), params_model.x_iec.value)*params_model.T_bl_iec.value

            # Hot spot to bottom liquid gradient
            t_hs_bl_model = 2*(params_model.T_ml_iec.value - params_model.T_bl_iec.value)*np.power(ts_result.load.iloc[i], params_model.y_iec.value) + params_model.T_hgr_iec.value*np.power(ts_result.load.iloc[i], params_model.y_iec.value)

            # Hot spot temperature
            ts_result.loc[i, 't_hs_model'] = t_bl_model + t_hs_bl_model

        return ts_result


