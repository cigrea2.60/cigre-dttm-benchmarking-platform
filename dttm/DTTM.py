# -*- coding: utf-8 -*-

import numpy as np

class DTTM:
    """
    DTTM mother class
    Represent a transformer thermal model
    """

    def __init__(self, longname, description, type_dttm, required_params=[], required_ts=[]):
        """
        Class constructor
        """
        self.shortname = self.__class__.__name__
        self.longname = longname
        self.description = description
        self.required_params = required_params
        self.required_ts = required_ts

        if type_dttm not in ["top_liquid", "hotspot"]:
            raise Exception("Unsupported DTTM type : %s"%type_dttm)
        self.type = type_dttm

    def evaluateModel(self, tts, **kwargs):
        """
        Evaluate model on TTS with the initial value
        Must be implemented in child class
        """
        print("==> Evaluation of DTTM %s"%self.shortname)
        lst_keep = []

        # Initialize the result dataframe
        ts_result = tts.ts_resampled.copy()
        if self.type == "top_liquid":
            ts_result["t_tl_model"] = np.nan
            lst_keep.append("t_tl_model")
            if "initial_value" in kwargs:
                ts_result.loc[0, 't_tl_model'] = kwargs["initial_value"]
            elif "duration_initial_values" in kwargs:
                idx_filter = ts_result.time < (ts_result.time.iloc[0] + kwargs["duration_initial_values"])
                ts_result.loc[idx_filter, "t_tl_model"] = ts_result.loc[idx_filter, "t_tl"]
            else:
                raise Exception("Initialization missing")
            if 't_tl' in ts_result:
                ts_result.drop(['t_tl'], axis=1, inplace=True)
        elif self.type == "hotspot":
            ts_result["t_hs_model"] = np.nan
            lst_keep.append("t_hs_model")
            if "initial_value" in kwargs:
                ts_result.loc[0, 't_hs_model'] = kwargs["initial_value"]
            elif "duration_initial_values" in kwargs:
                idx_filter = ts_result.time < (ts_result.time.iloc[0] + kwargs["duration_initial_values"])
                ts_result.loc[idx_filter, "t_hs_model"] = ts_result.loc[idx_filter, "t_hs"]
            else:
                raise Exception("Initialization missing")
            if 't_hs' in ts_result:
                ts_result.drop(['t_hs'], axis=1, inplace=True)

        # Keep only timeseries
        if "lst_ts_to_keep" in kwargs:
            ts_result = ts_result[["time"] + kwargs["lst_ts_to_keep"] + lst_keep]

        return ts_result

    def isEvaluationPossible(self, tts, lst_ts_to_keep):
        """
        Check if the DTTM evaluation is possible on the TTS
        """
        # Parameters
        lst_missing_params = []
        params_full = list(tts.params['main'].keys())
        if self.shortname in tts.params.keys():
            params_full += list(tts.params[self.shortname].keys())
        for p in self.required_params:
            if not p in params_full:
                lst_missing_params.append(p)

        # Timeseries
        lst_missing_ts = []
        for ts in self.required_ts:
            if not ts in lst_ts_to_keep:
                lst_missing_ts.append(ts)

        if len(lst_missing_params) > 0:
            print("=> Missing parameters: %s"%lst_missing_params)
        if len(lst_missing_ts) > 0:
            print("=> Missing timeseries: %s"%lst_missing_ts)

        if len(lst_missing_params) > 0 or len(lst_missing_ts) > 0:
            return False
        return True

