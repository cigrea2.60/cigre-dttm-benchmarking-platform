# -*- coding: utf-8 -*-

import numpy as np

from .DTTM import DTTM

class DSusa_top_liquid(DTTM):
    """
    D. Susa (2005) Top liquid model
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "D. Susa top liquid model",
                         description = "D. Susa (2005) top liquid model",
                         type_dttm = "top_liquid",
                         required_params=["cooling_mode", "l_material", "TR_tl_r", "t_a_r", "w_material", "P_dc_pu", "P_a_pu", "Tau_tl_r", "R"],
                         required_ts=['t_amb', 'load'])

    def evaluateModel(self, tts, **kwargs):
        """
        Evaluate model on TTS with the initial value
        """

        # Call mother method for initialization
        ts_result = super().evaluateModel(tts, **kwargs)

        params_model = tts.params[self.shortname]

        deltaT = ts_result.time.diff().dt.total_seconds() #s

        # Table 3.4 (initial oil circulation speed > 0(transformer on load))
        if params_model.cooling_mode.value == "ONAN":
            n = 0.25
        elif params_model.cooling_mode.value == "ONAF":
            n = 0.2
        elif params_model.cooling_mode.value == "OFAF":
            n = 0.2
        else:
            raise Exception("Cooling mode not implemented: %s"%params_model.cooling_mode.value)

        # Oil viscosity - Appendix A
        if params_model.l_material.value == "oil":
            A1 = 0.13573e-5
            A2 = 2797.3
        elif params_model.l_material.value == "silicone":
            A1 = 0.12127e-3
            A2 = 1782.3
        else:
            raise Exception("Liquid material not implemented: %s"%params_model.l_material.value)
        # (3.12) + (3.16)
        def mu(t_tl):
            return A1*np.exp(A2/(t_tl + 273))
        def mu_pu(t_tl):
            return mu(t_tl)/mu(params_model.TR_tl_r.value + params_model.t_a_r.value)

        # Losses temperature correction with material - (3.22)
        if params_model.w_material.value == "copper":
            t_k = 235
        elif params_model.w_material.value == "aluminium":
            t_k = 225
        else:
            raise Exception("Winding material not implemented: %s"%params_model.w_material.value)
        t_e_r = params_model.TR_tl_r.value + params_model.t_a_r.value

        for i in range(1, len(ts_result)):

            # Load losses with temperature dependence (3.22)
            # Windings hot-spot not available, take top liquid temperature instead
            Pl_pu = params_model.P_dc_pu.value*(ts_result.t_tl_model.iloc[i-1] + t_k)/(t_e_r + t_k) + params_model.P_a_pu.value*(t_e_r + t_k)/(ts_result.t_tl_model.iloc[i-1] + t_k)

            # To avoid some bugs
            if ts_result.t_tl_model.iloc[i-1] < ts_result.t_amb.iloc[i]:
                ts_result.loc[i-1, 't_tl_model'] = ts_result.t_amb.iloc[i]

            # (3.24) §3.2.1 + Euler 1st order
            ts_result.loc[i, 't_tl_model'] = ts_result.t_tl_model.iloc[i-1] + deltaT.iloc[i]/(params_model.Tau_tl_r.value*60.0)*(
                (1 + params_model.R.value*Pl_pu*np.power(ts_result.load.iloc[i], 2))/(1 + params_model.R.value)*params_model.TR_tl_r.value -
                np.power(ts_result.t_tl_model.iloc[i-1] - ts_result.t_amb.iloc[i], 1 + n)/np.power(params_model.TR_tl_r.value*mu_pu(ts_result.t_tl_model.iloc[i-1]), n))

        return ts_result


