# -*- coding: utf-8 -*-

import numpy as np

from .DTTM import DTTM

class IEC60076_VCS1tl_hotspot(DTTM):
    """
    IEC 60076-7 (2017) hotspot model from the top liquid temperature, with Variable Cooling Stage improvement
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "IEC 60076-7 VCS1tl hotspot model",
                         description = "IEC 60076-7 hotspot model with modification \
                         to take into account the cooling stage (oil time constant \
                         depends on the cooling stage), from the top liquid temperature",
                         type_dttm = "hotspot",
                         required_params=['k21', 'k22', 'Tau_hgr_iec', 'Tau_tl_0_iec', 'Tau_tl_1_iec', 'Tau_tl_2_iec', 'T_hgr_iec', 'y_iec'],
                         required_ts=['t_tl', 'load', 'cooling_stage'])

    def evaluateModel(self, tts, **kwargs):
        """
        Evaluate model on TTS with the initial value
        """

        # Call mother method for initialization
        ts_result = super().evaluateModel(tts, **kwargs)
        i_first = ts_result.t_hs_model.isna().idxmax()

        # Others initialization
        dTh1 = np.empty(len(ts_result.time))
        dTh1[:] = np.nan
        dTh2 = dTh1.copy()
        dTh = dTh1.copy()
        dTh[0:i_first] = ts_result.t_hs_model.iloc[0:i_first] - ts_result.t_tl.iloc[0:i_first]
        dTh1[0:i_first] = dTh[0:i_first]
        dTh2[0:i_first] = 0

        params_model = tts.params[self.shortname]

        deltaT = ts_result.time.diff().dt.total_seconds() #s

        for i in range(i_first, len(ts_result)):

            # Top-liquid time constant depend on the cooling stage
            if ts_result.cooling_stage.iloc[i] == 0:
                Tau_tl = params_model.Tau_tl_0_iec.value
            elif ts_result.cooling_stage.iloc[i] == 1:
                Tau_tl = params_model.Tau_tl_1_iec.value
            elif ts_result.cooling_stage.iloc[i] == 2:
                Tau_tl = params_model.Tau_tl_2_iec.value
            else:
                raise Exception("Unknown cooling stage !: %s"%ts_result.cooling_stage.iloc[i])

            # Δθh1 (20) §8.2.3
            DdTh1 = deltaT.iloc[i]/(params_model.k22.value*params_model.Tau_hgr_iec.value*60.0)*(params_model.k21.value*params_model.T_hgr_iec.value*np.power(ts_result.load.iloc[i], params_model.y_iec.value) - dTh1[i-1])
            dTh1[i] = dTh1[i-1] + DdTh1
            # Δθh2 (21) §8.2.3
            DdTh2 = deltaT.iloc[i]/((1/params_model.k22.value)*Tau_tl*60.0)*((params_model.k21.value - 1)*params_model.T_hgr_iec.value*np.power(ts_result.load.iloc[i], params_model.y_iec.value) - dTh2[i-1])
            dTh2[i] = dTh2[i-1] + DdTh2
            # Δθh (22) §8.2.3
            dTh[i] = dTh1[i] + dTh2[i]
            # (23) §8.2.3
            ts_result.loc[i, 't_hs_model'] = ts_result.t_tl.iloc[i] + dTh[i]

        return ts_result

    def evaluateSteadyStateModel(self, tts, **kwargs):
        """
        Evaluate steady state model on TTS with the initial value
        """

        # Call mother method for initialization
        ts_result = super().evaluateModel(tts, **kwargs)
        i_first = ts_result.t_hs_model.isna().idxmax()

        params_model = tts.params[self.shortname]

        for i in range(i_first, len(ts_result)):

            # Steady-state model is calculated considering dTheta/dt=0 in (6,7,8,9) §8.2.1
            ts_result.loc[i, 't_hs_model'] = ts_result.t_tl.iloc[i] + params_model.T_hgr_iec.value*np.power(ts_result.load.iloc[i], params_model.y_iec.value)

        return ts_result


