# -*- coding: utf-8 -*-

import numpy as np

from .DTTM import DTTM

class IEC60076_evolution_top_liquid(DTTM):
    """
    IEC 60076-7 Top liquid model, proposal for evolution
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "IEC 60076-7 top liquid model proposal for evolution",
                         description = "IEC 60076-7 top liquid model proposal for evolution",
                         type_dttm = "top_liquid",
                         required_params=["cooling_mode", "l_material", "w_material", 'x_iec_76_2', 't_a_r',
                                          'Tau_tl_0_iec', 'Tau_tl_1_iec', 'Tau_tl_2_iec',
                                          'T_tl_0_iec', 'T_tl_1_iec', 'T_tl_2_iec',
                                          'tap_pos_r', 'tap_pos_minus', 'tap_pos_plus',
                                          'P_dc_minus', 'P_dc_r', 'P_dc_rp1', 'P_dc_plus', 'P_dc_trt',
                                          'P_a_minus', 'P_a_r', 'P_a_rp1', 'P_a_plus', 'P_a_trt',
                                          'P_0',
                                          'VFT_A', 'VFT_B', 'VFT_C'],
                         required_ts=['t_amb', 'load', 'tap_pos', 'cooling_stage'])

    def evaluateModel(self, tts, **kwargs):
        """
        Evaluate model on TTS with the initial value
        """

        # Call mother method for initialization
        ts_result = super().evaluateModel(tts, **kwargs)
        i_first = ts_result.t_tl_model.isna().idxmax()

        params_model = tts.params[self.shortname]

        deltaT = ts_result.time.diff().dt.total_seconds() #s

        # Table 3.4 (initial oil circulation speed > 0(transformer on load))
        if params_model.cooling_mode.value == "ONAN":
            n = 0.25
        elif params_model.cooling_mode.value == "ONAF":
            n = 0.2
        elif params_model.cooling_mode.value == "OFAF":
            n = 0.2
        else:
            raise Exception("Cooling mode not implemented: %s"%params_model.cooling_mode.value)

        ### VFT: Oil viscosity
        def mu(t_tl):
            # t_tl in °C
            return params_model.VFT_A.value*np.exp(params_model.VFT_B.value/(t_tl + 273 - params_model.VFT_C.value))
        def mu_pu(t_tl):
            return mu(t_tl)/mu(params_model.T_tl_2_iec.value + params_model.t_a_r.value)

        # Losses temperature correction with material - D. Susa (3.22)
        if params_model.w_material.value == "copper":
            t_k = 235
        elif params_model.w_material.value == "aluminium":
            t_k = 225
        else:
            raise Exception("Winding material not implemented: %s"%params_model.w_material.value)
        t_e_r = params_model.T_tl_2_iec.value + params_model.t_a_r.value

        ### Ratio of losses
        # Load to no-load
        R_r = (params_model.P_dc_r.value + params_model.P_a_r.value)/params_model.P_0.value
        R_rp1 = (params_model.P_dc_rp1.value + params_model.P_a_rp1.value)/params_model.P_0.value
        R_minus = (params_model.P_dc_minus.value + params_model.P_a_minus.value)/params_model.P_0.value
        R_plus = (params_model.P_dc_plus.value + params_model.P_a_plus.value)/params_model.P_0.value
        R_trt = (params_model.P_dc_trt.value + params_model.P_a_trt.value)/params_model.P_0.value
        # Additionnal to DC
        Rp_r = params_model.P_a_r.value/params_model.P_dc_r.value
        Rp_rp1 = params_model.P_a_rp1.value/params_model.P_dc_rp1.value
        Rp_minus = params_model.P_a_minus.value/params_model.P_dc_minus.value
        Rp_plus = params_model.P_a_plus.value/params_model.P_dc_plus.value

        for i in range(i_first, len(ts_result)):

            ### VCS: Time constant and top-liquid temperature rise depend on the cooling stage
            if ts_result.cooling_stage.iloc[i] == 0:
                T_tl_VCS = params_model.T_tl_0_iec.value
                Tau_tl_VCS = params_model.Tau_tl_0_iec.value
            elif ts_result.cooling_stage.iloc[i] == 1:
                T_tl_VCS = params_model.T_tl_1_iec.value
                Tau_tl_VCS = params_model.Tau_tl_1_iec.value
            elif ts_result.cooling_stage.iloc[i] == 2:
                T_tl_VCS = params_model.T_tl_2_iec.value
                Tau_tl_VCS = params_model.Tau_tl_2_iec.value
            else:
                raise Exception("Unknown cooling stage !: %s"%ts_result.cooling_stage.iloc[i])

            ### VTP: Dependance of the losses on the tap position
            if ts_result.tap_pos.iloc[i] <= params_model.tap_pos_r.value:
                # IEC 60076-7:2018-01 §9.2
                m1 = (R_r - R_minus)/(params_model.tap_pos_r.value - params_model.tap_pos_minus.value)
                # IEC 60076-7:2018-01 §9.3 (30)
                R_VTP = R_r + (ts_result.tap_pos.iloc[i] - params_model.tap_pos_r.value)*m1
                # DC, Additionnal loss separation
                m1p = (Rp_r - Rp_minus)/(params_model.tap_pos_r.value - params_model.tap_pos_minus.value)
                Rp_VTP = Rp_r + (ts_result.tap_pos.iloc[i] - params_model.tap_pos_r.value)*m1p
            else:
                # IEC 60076-7:2018-01 §9.2
                m2 = (R_plus - R_rp1)/(params_model.tap_pos_plus.value - (params_model.tap_pos_r.value + 1))
                # IEC 60076-7:2018-01 §9.3 (29)
                R_VTP = R_rp1 + (ts_result.tap.iloc[i] - (params_model.tap_pos_r.value + 1))*m2
                # DC, Additionnal loss separation
                m2p = (Rp_plus - Rp_rp1)/(params_model.tap_pos_plus.value - (params_model.tap_pos_r.value + 1))
                Rp_VTP = Rp_rp1 + (ts_result.tap.iloc[i] - (params_model.tap_pos_r.value + 1))*m2p

            # Dependance of the top-liquid temperature rise on the tap position, IEC 60076-2:2011-02 §7.13
            T_tl_VCS_VTP = T_tl_VCS*np.power((1 + R_VTP)/(1 + R_trt), params_model.x_iec_76_2.value)

            # Dependance of the top-liquid time constant on the tap position, from IEC 60076-7:2018-01 §E + see article
            Tau_tl_VCS_VTP = (T_tl_VCS_VTP/T_tl_VCS)*((1 + R_trt)/(1 + R_VTP))*Tau_tl_VCS

            ### Load losses with temperature dependence
            P_dc_VTP = params_model.P_0.value*R_VTP/(1 + Rp_VTP)
            P_a_VTP = Rp_VTP*P_dc_VTP
            P_dc_VTP_T = P_dc_VTP*(ts_result.t_tl_model.iloc[i-1] + t_k)/(t_e_r + t_k)
            P_a_VTP_T = P_a_VTP*(t_e_r + t_k)/(ts_result.t_tl_model.iloc[i-1] + t_k)
            P_l_VTP_T = P_dc_VTP_T + P_a_VTP_T
            R_VTP_T = P_l_VTP_T/params_model.P_0.value
            P_l_VTP_T_pu = P_l_VTP_T/(P_l_VTP_T + params_model.P_0.value)

            # To avoid some bugs
            if ts_result.t_tl_model.iloc[i-1] < ts_result.t_amb.iloc[i]:
                ts_result.loc[i-1, 't_tl_model'] = ts_result.t_amb.iloc[i]

            # D. Susa (3.24) §3.2.1 + Euler 1st order
            ts_result.loc[i, 't_tl_model'] = ts_result.t_tl_model.iloc[i-1] + deltaT.iloc[i]/(Tau_tl_VCS_VTP*60.0)*(
                (1 + R_VTP_T*P_l_VTP_T_pu*np.power(ts_result.load.iloc[i], 2))/(1 + R_VTP_T)*T_tl_VCS_VTP -
                np.power(ts_result.t_tl_model.iloc[i-1] - ts_result.t_amb.iloc[i], 1 + n)/np.power(T_tl_VCS_VTP*mu_pu(ts_result.t_tl_model.iloc[i-1]), n))

        return ts_result


