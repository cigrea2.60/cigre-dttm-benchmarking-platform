# -*- coding: utf-8 -*-

import numpy as np

from .DTTM import DTTM

class IEEEC57_91_G_top_liquid(DTTM):
    """
    IEEE C57.91 Annex G (2011) top-liquid model
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "IEEE C57.91 Annex G top liquid model",
                         description = "IEEE C57.91 Annex G (2011) top liquid model",
                         type_dttm = "top_liquid",
                         required_params=["P_w", "P_e", "P_s", "P_c_r", "Dt_w_r", "Dt_to_r", "Dt_bo_r", "t_a_r", "w_material", "tau_w", "M_cc", "M_tank", "l_material", "M_oil", "excitation", "P_c_oe", "cooling_mode"],
                         required_ts=['t_amb', 'load'])

    def evaluateModel(self, tts, **kwargs):
        """
        Evaluate model on TTS with the initial value
        """

        # Call mother method for initialization
        ts_result = super().evaluateModel(tts, **kwargs)
        i_first = ts_result.t_tl_model.isna().idxmax()

        params_model = tts.params[self.shortname]

        deltaT = ts_result.time.diff().dt.total_seconds()/60 # min

        # Initialization of the other timeseries needed for this evaluation (hypothesis...)
        ts_result["t_bl_model"] = np.nan
        ts_result.loc[0:i_first-1, 't_bl_model'] = ts_result.t_amb.iloc[0:i_first]
        if 't_bl' in ts_result:
            ts_result.drop(['t_bl'], axis=1, inplace=True)
        ts_result["t_al_model"] = np.nan
        ts_result.loc[0:i_first-1, 't_al_model'] = (ts_result.t_bl_model.iloc[0:i_first] + ts_result.t_tl_model.iloc[0:i_first])/2
        if 't_al' in ts_result:
            ts_result.drop(['t_al'], axis=1, inplace=True)
        ts_result["t_w_model"] = np.nan
        ts_result.loc[0:i_first-1, 't_w_model'] = ts_result.t_tl_model.iloc[0:i_first]
        if 't_w' in ts_result:
            ts_result.drop(['t_w'], axis=1, inplace=True)

        # Table G.2—Specific heat and constants for viscosity calculation
        if params_model.w_material.value == "copper":
            t_k = 234.5 # (Line 470 of the program listing)
            Cp_w = 2.91
        elif params_model.w_material.value == "aluminium":
            t_k = 225.0 # (Line 450 of the program listing)
            Cp_w = 6.798
        else:
            raise Exception("Winding material not implemented: %s"%params_model.w_material.value)
        Cp_tank = 3.51
        Cp_core = 3.51
        if params_model.l_material.value == "oil":
            Cp_oil = 13.92
            D_oil = 0.0013573
            G_oil = 2797.3
        elif params_model.l_material.value == "silicone":
            Cp_oil = 11.49
            D_oil = 0.12127
            G_oil = 1782.3
        elif params_model.l_material.value == "HTHC":
            Cp_oil = 14.55
            D_oil = 0.00007343
            G_oil = 4434.7
        else:
            raise Exception("Liquid material not implemented: %s"%params_model.l_material.value)

        # (G.28) viscosity of oil
        def mu(t):
            return D_oil*np.exp(G_oil/(t + 273))

        # Table G.3—Summary of exponents
        if params_model.cooling_mode.value == "ONAN":
            x = 0.5
            y = 0.8
            z = 0.5
            Dt_do_r = params_model.Dt_to_r.value # (Line 650 of the program listing)
        elif params_model.cooling_mode.value == "ONAF":
            x = 0.5
            y = 0.9
            z = 0.5
            Dt_do_r = params_model.Dt_to_r.value # (Line 680 of the program listing)
        elif params_model.cooling_mode.value == "OFAF":
            x = 0.5
            y = 0.9
            z = 1.0
            Dt_do_r = params_model.Dt_w_r.value # (Line 710 of the program listing)
        elif params_model.cooling_mode.value == "ODAF":
            x = 1.0
            y = 1.0
            z = 1.0
            Dt_do_r = params_model.Dt_to_r.value # (Line 740 of the program listing)
        else:
            raise Exception("Cooling mode not implemented: %s"%params_model.cooling_mode.value)

        # Initial value
        t_tdo = ts_result.t_tl_model.iloc[i_first-1]
        # (Line 860 of the program listing)
        t_w_r = params_model.t_a_r.value + params_model.Dt_w_r.value
        t_to_r = params_model.t_a_r.value + params_model.Dt_to_r.value
        # (Line 880 of the program listing)
        t_bo_r = params_model.t_a_r.value + params_model.Dt_bo_r.value
        t_tdo_r = params_model.t_a_r.value + Dt_do_r
        # (Line 900 of the program listing)
        t_dao_r = (t_tdo_r + t_bo_r)/2
        t_ao_r = (t_to_r + t_bo_r)/2 # TFAVER in listing

        # Table G.1
        mu_w_r = mu((t_w_r + t_dao_r)/2)

        # (G.7) winding mass times specific heat
        MwCpw = ((params_model.P_w.value + params_model.P_e.value)*params_model.tau_w.value)/(t_w_r - t_dao_r)
        # (G.22) mass of windings
        M_w = MwCpw/Cp_w
        # (G.23) mass of core
        M_core = params_model.M_cc.value - M_w
        # (G.24) total mass times specific heat of oil, tank, and core
        MCp = params_model.M_tank.value*Cp_tank + M_core*Cp_core + params_model.M_oil.value*Cp_oil

        for i in range(i_first, len(ts_result)):
            ## §G.3.2 Average winding temperature
            # (G.5) temperature correction for losses of winding
            K_w = (ts_result.t_w_model.iloc[i-1] + t_k) / (t_w_r + t_k)
            # (G.4) heat generated by the windings
            Q_gen_w = np.power(ts_result.load.iloc[i], 2)*(params_model.P_w.value*K_w + params_model.P_e.value/K_w)*deltaT.iloc[i]
            # (Line 1650 of the program listing)
            t_dao = (t_tdo + ts_result.t_bl_model.iloc[i-1])/2
            # (Line 1680, 1750 of the program listing)
            if ts_result.t_w_model.iloc[i-1] < t_dao:
                Q_lost_w = 0
            else:
                if params_model.cooling_mode.value in ["ONAN", "ONAF", "OFAF"]:
                    # Table G.1
                    mu_w = mu((ts_result.t_w_model.iloc[i-1] + t_dao)/2)
                    # (G.6A) heat lost by the windings
                    Q_lost_w = np.power((ts_result.t_w_model.iloc[i-1] - t_dao)/(t_w_r - t_dao_r), 5/4)*\
                                np.power(mu_w_r/mu_w, 1/4)*(params_model.P_w.value + params_model.P_e.value)*deltaT.iloc[i]
                elif params_model.cooling_mode.value == "ODAF":
                    # (G.6B) heat lost by the windings
                    Q_lost_w = (ts_result.t_w_model.iloc[i-1] - t_dao)/(t_w_r - t_dao_r)*\
                                (params_model.P_w.value + params_model.P_e.value)*deltaT.iloc[i]
                else:
                    raise Exception("Cooling mode not implemented: %s"%params_model.cooling_mode.value)
            # (Line 1760 of the program listing)
            if ts_result.t_w_model.iloc[i-1] < ts_result.t_bl_model.iloc[i-1]:
                ts_result.loc[i-1, 't_w_model'] = ts_result.t_bl_model.iloc[i-1]
            # (G.8) average winding temperature
            ts_result.loc[i, 't_w_model'] = (Q_gen_w - Q_lost_w + MwCpw*ts_result.t_w_model.iloc[i-1])/MwCpw

            ## G.3.3 Winding duct oil temperature rise over bottom oil
            # (G.9) temperature rise of fluid at top of duct over bottom fluid
            Dt_dobo = np.power(Q_lost_w/((params_model.P_w.value + params_model.P_e.value)*deltaT.iloc[i]), x)*(t_tdo_r - t_bo_r)
            t_tdo = Dt_dobo + ts_result.t_bl_model.iloc[i-1]

            ## G.3.5 Average oil temperature
            if params_model.excitation.value == "normal":
                # (G.18A)
                Q_c = params_model.P_c_r.value*deltaT.iloc[i]
            elif params_model.excitation.value == "over":
                # (G.18B)
                Q_c = params_model.P_c_oe.value*deltaT.iloc[i]
            else:
                raise Exception("Excitation not implemented: %s"%params_model.excitation.value)
            # (G.19) heat generated by the stray loss
            Q_s = (np.power(ts_result.load.iloc[i], 2)*params_model.P_s.value/K_w)
            # (G.20) total losses at rated load
            if params_model.excitation.value == "normal":
                P_t = params_model.P_w.value + params_model.P_e.value + params_model.P_s.value + params_model.P_c_r.value
            elif params_model.excitation.value == "over":
                P_t = params_model.P_w.value + params_model.P_e.value + params_model.P_s.value + params_model.P_c_oe.value
            else:
                raise Exception("Excitation not implemented: %s"%params_model.excitation.value)
            # (G.21) heat lost by fluid to ambient
            Q_lost_o = np.power((ts_result.t_al_model.iloc[i-1] - ts_result.t_amb.iloc[i-1])/(t_ao_r - params_model.t_a_r.value), 1/y)*\
                        P_t*deltaT.iloc[i]
            # (G.25) average fluid temperature in tank and radiator at the next instant of time
            ts_result.loc[i, 't_al_model'] = (Q_lost_w + Q_s + Q_c - Q_lost_o + MCp*ts_result.t_al_model.iloc[i-1])/MCp

            ## G.3.6 Top and bottom oil temperatures
            # (G.26) temperature rise of oil at top of radiator over bottom fluid
            Dt_tb = np.power(Q_lost_o/(P_t*deltaT.iloc[i]), z)*(t_to_r - t_bo_r)
            # (G.2) bottom fluid temperature
            ts_result.loc[i, 't_bl_model'] = ts_result.t_al_model.iloc[i] - Dt_tb/2
            # (Line 2000 of the program listing)
            if ts_result.t_bl_model.iloc[i] < ts_result.t_amb.iloc[i]:
                ts_result.loc[i, 't_bl_model'] = ts_result.t_amb.iloc[i]
            # (Line 2010 of the program listing)
            if t_tdo < ts_result.t_bl_model.iloc[i]:
                t_tdo = ts_result.t_bl_model.iloc[i]
            # (G.3) top fluid temperature
            ts_result.loc[i, 't_tl_model'] = ts_result.t_al_model.iloc[i] + Dt_tb/2

        return ts_result


