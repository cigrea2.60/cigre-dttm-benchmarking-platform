# -*- coding: utf-8 -*-

import numpy as np

from .DTTM import DTTM

class DSusa_hotspot(DTTM):
    """
    D. Susa (2005) hotspot model
    """

    def __init__(self):
        """
        Class constructor
        """
        super().__init__(longname = "D. Susa hotspot model",
                         description = "D. Susa (2005) hotspot model",
                         type_dttm = "hotspot",
                         required_params=["cooling_mode", "l_material", "TR_hstl_r", "TR_tl_r", "t_a_r", "w_material", "P_dc_pu", "P_e_pu", "Tau_w_r"],
                         required_ts=['t_tl', 'load'])

    def evaluateModel(self, tts, **kwargs):
        """
        Evaluate model on TTS with the initial value
        """

        # Call mother method for initialization
        ts_result = super().evaluateModel(tts, **kwargs)

        params_model = tts.params[self.shortname]

        deltaT = ts_result.time.diff().dt.total_seconds() #s

        # Table 3.6 (transformer on load with external cooling)
        if params_model.cooling_mode.value in ["ONAN", "ONAF", "OFAF"]:
            n = 0.5
            n2 = 0.1
        else:
            raise Exception("Cooling mode not implemented: %s"%params_model.cooling_mode.value)

        # Oil viscosity - Appendix A
        if params_model.l_material.value == "oil":
            A1 = 0.13573e-5
            A2 = 2797.3
        elif params_model.l_material.value == "silicone":
            A1 = 0.12127e-3
            A2 = 1782.3
        else:
            raise Exception("Liquid material not implemented: %s"%params_model.l_material.value)
        # (3.12) + (3.16)
        def mu(t_tl):
            return A1*np.exp(A2/(t_tl + 273))
        def mu_pu(t_tl):
            return mu(t_tl)/mu(params_model.TR_tl_r.value + params_model.t_a_r.value)

        # Losses temperature correction with material - (3.37)
        if params_model.w_material.value == "copper":
            t_k = 235
        elif params_model.w_material.value == "aluminium":
            t_k = 225
        else:
            raise Exception("Winding material not implemented: %s"%params_model.w_material.value)
        t_hs_r = params_model.TR_hstl_r.value + params_model.TR_tl_r.value + params_model.t_a_r.value

        for i in range(1, len(ts_result)):

            # Winding loss’s dependence on temperature (3.37)
            Pw_pu = params_model.P_dc_pu.value*(ts_result.t_hs_model.iloc[i-1] + t_k)/(t_hs_r + t_k) + params_model.P_e_pu.value*(t_hs_r + t_k)/(ts_result.t_hs_model.iloc[i-1] + t_k)

            # To avoid some bugs
            if ts_result.t_hs_model.iloc[i-1] < ts_result.t_tl.iloc[i]:
                ts_result.loc[i-1, 't_hs_model'] = ts_result.t_tl.iloc[i]

            # (3.41) §3.2.2 + Euler 1st order
            ts_result.loc[i, 't_hs_model'] = ts_result.t_hs_model.iloc[i-1] + deltaT.iloc[i]/(params_model.Tau_w_r.value*60.0)*(
                np.power(ts_result.load.iloc[i], 2)*Pw_pu*params_model.TR_hstl_r.value -
                np.power(ts_result.t_hs_model.iloc[i-1] - ts_result.t_tl.iloc[i], 1 + n2)/(np.power(params_model.TR_hstl_r.value, n2)*np.power(mu_pu(ts_result.t_tl.iloc[i]), n)))

        return ts_result


