# -*- coding: utf-8 -*-

import os, shutil
import pandas as pd
from openpyxl import load_workbook

from .DTTM import DTTM

class DTTMProprietary(DTTM):
    """
    Proprietary DTTM mother class
    """

    def __init__(self, longname, description, type_dttm, required_ts):
        """
        Class constructor
        """
        super().__init__(longname, description, type_dttm, required_ts=required_ts)

    def evaluateModel(self, tts, **kwargs):
        """
        Evaluate model on TTS with the initial value
        """
        # Call mother method for initialization
        ts_result = super().evaluateModel(tts, **kwargs)

        # Path to export / import data
        path_data = os.path.join(os.path.dirname(tts.path_timeserie), "proprietary_dttm", self.shortname)
        if not os.path.exists(path_data):
            os.makedirs(path_data)

        ### Write timeseries to evaluate
        path_out = os.path.join(path_data, "%s_evaluation_timeseries_TO_FILL.xlsx"%tts.id)
        tts.saveTimeserieToExcel(ts_result, path_out)

        ### Write main parameters
        params_model = {}
        for model in tts.params.keys():
            params_model.update(tts.params[model])
        df_params = pd.DataFrame(params_model).T
        df_params = df_params[df_params.to_distribute == 'yes']
        df_params.drop('to_distribute', axis=1, inplace=True)

        # Sort by shortnames
        df_params.reset_index(inplace=True)
        df_params.rename({"index": "shortname"}, axis=1, inplace=True)
        df_params.sort_values(by=['shortname'], inplace=True)

        # Save parameters
        path_out = os.path.join(path_data, "%s_evaluation_parameters.xlsx"%tts.id)
        shutil.copyfile(tts.path_parameters, path_out)
        wb_out = load_workbook(path_out, data_only=True)
        for ws_name in wb_out.sheetnames:
            if not ws_name in ["About DTTM-BP", "TTS description"]:
                std = wb_out.get_sheet_by_name(ws_name)
                wb_out.remove_sheet(std)
        wb_out.save(path_out)

        with pd.ExcelWriter(path_out, mode='a', engine="openpyxl") as writer:
            df_params.to_excel(writer, index=False, sheet_name="TTS parameters")

        ### Import result file
        path_in = os.path.join(path_data, "%s_evaluation_timeseries_FILLED.xlsx"%tts.id)
        if os.path.exists(path_in):
            print('Load data for proprietary model %s'%self.shortname)
            ts_result = pd.read_excel(path_in, index_col=None)
            # Round times to closest second to avoid problems linked to export/import
            ts_result['time'] = ts_result['time'].dt.round('1s')
        else:
            print("Proprietary model %s hasn't results yet... %s doesn't exist"%(self.shortname, path_in))
            return pd.DataFrame()

        return ts_result


