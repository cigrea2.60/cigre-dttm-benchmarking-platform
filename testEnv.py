import sys
import matplotlib
from PyQt5.QtCore import QT_VERSION_STR, PYQT_VERSION_STR
import pandas
import openpyxl
import xlsxwriter
if sys.version_info >= (3, 8):
    from importlib import metadata as importlib_metadata
else:
    import importlib_metadata

print("sys.version", sys.version)
print("sys.path", sys.path)
print("matplotlib.__version__", matplotlib.__version__)
print("QT_VERSION_STR: ", QT_VERSION_STR, "\PYQT_VERSION_STR: ", PYQT_VERSION_STR)
print("pandas.__version__", pandas.__version__)
print("openpyxl.__version__", openpyxl.__version__)
print("xlsxwriter.__version__", xlsxwriter.__version__)

dists = importlib_metadata.distributions()
for dist in dists:
    name = dist.metadata["Name"]
    version = dist.version
    license = dist.metadata["License"]
    print(f'found distribution {name}=={version}')

