# -*- coding: utf-8 -*-

import os
import pandas as pd
import numpy as np
import datetime as dt

import matplotlib
try:
    import matplotlib.backends.backend_qt5agg
except:
    pass
else:
    matplotlib.use('Qt5Agg')
from matplotlib.ticker import PercentFormatter, MaxNLocator, FormatStrFormatter
import matplotlib.pyplot as plt
plt.close("all")

from tts import TTS

# =============================================================================
# DTTM-BP
# =============================================================================

### Paths definition
path_tts = os.path.join(".", "tts")
# path_tts = os.path.join("..", "database", "tts") # For DTTM-BP Developpers only
path_dttm = os.path.join(".", "dttm")

### TTS to load
id_tfo = "a"
id_input_ds = "01"

# List of timeseries to keep for the evaluation
# Top liquid
lst_ts_to_keep_tl = ['t_amb', 'load', 'cooling_stage', 'tap_pos']
# Hot spot
lst_ts_to_keep_hs = ['t_amb', 't_tl', 'load', 'cooling_stage', 'tap_pos']

### DTTMs to evaluate
# - List of DTTMs shortnames, example ['IEC60076_top_liquid', 'IEC60076_hotspot']
# - "all" to evaluate all the DTTMs in the ./dttm folder
# - [] to only process the TTS
#lst_dttm = []
#lst_dttm = ['IEC60076_hotspot','IEEEC57_91_G_hotspot']
#lst_dttm = ['IEC60076_top_liquid','IEC60076_evolution_top_liquid', 'DSusa_top_liquid']
#lst_dttm = ['IEEEC57_91_G_top_liquid', 'IEEEC57_91_G_hotspot']
#lst_dttm = ['IEC60076_top_liquid', 'IEC60076_hotspot',
#            'IEEEC57_91_G_top_liquid', 'IEEEC57_91_G_hotspot',
#            'DSusa_top_liquid', 'DSusa_hotspot',
#            'THAM_top_liquid', 'THAM_hotspot',
#            'LoadGT_IEC_top_liquid', 'LoadGT_IEEE_top_liquid', 'LoadGT_IEC_hotspot']
# lst_dttm = ['IEC60076_top_liquid',
#              'IEEEC57_91_G_top_liquid',
#              'THAM_top_liquid',
#              'LoadGT_IEC_top_liquid', 'LoadGT_IEEE_top_liquid']
lst_dttm = ['IEC60076_hotspot',
            'ASNZS60076_hotspot']

### Add graph title prefix
add_graph_title = "top-liquid "

### DTTMs graphs to show
# List of choices between ["comparison_graph", "individual_graph"]
lst_dttm_graphs_to_show = ["comparison_graph"]

### Metrics
evaluate_filtered_metrics = False # on-load, LDM, LFM

### Timestep for TTS, in seconds
tts_timestep = 10*60

### Duration initial values for DTTM evaluation
duration_initial_values = dt.timedelta(seconds=24*60*60)

# Show figures?
show_figures = True

# Plot original timeseries?
plot_original_ts = True

# Let's go!
if not show_figures:
    plt.ioff()

# =============================================================================
# Load TTS
# =============================================================================
print("\n===== TTS loading =====")

for f in os.listdir(os.path.join(path_tts, "TTS%s"%id_tfo)):
    if "TTS%s%s-timeseries"%(id_tfo, id_input_ds) in f and ".xlsx" in f and not "~$" in f:
        path_timeserie = os.path.join(path_tts, "TTS%s"%id_tfo, f)
    elif "TTS%s-parameters"%id_tfo in f and ".xlsx" in f and not "~$" in f:
        path_parameters = os.path.join(path_tts, "TTS%s"%id_tfo, f)
tts = TTS.TTS("TTS%s%s"%(id_tfo, id_input_ds), path_parameters, path_timeserie)

# Resample data (if data isn't resampled, there is a risk of having NaN values due to the different timeseries in the Excel files)
tts.resampleTimeserie(tts_timestep)

# Plot data
if plot_original_ts:
    fig = tts.plot(tts.ts_resampled, label_title="Original timeseries",
            lst_first_axis=['t_amb', 't_bl', 't_tl', 't_hs'], label_first_axis="Temperature", mpl_options_first_axis={"major_formatter": FormatStrFormatter("%.0f°C")},
            lst_second_axis=['load'], label_second_axis="Load factor", y_lim_second_axis={"bottom": 0, "top": 1.5}, mpl_options_second_axis={"major_formatter": PercentFormatter(xmax=1, decimals=0)},
            lst_third_axis=['tap_pos', 'cooling_stage'], label_third_axis="Tap position | Cooling stage", mpl_options_third_axis={"major_locator": MaxNLocator(integer=True)})
    fig.savefig(os.path.join(tts.path_tts, "%s - original timeseries.png"%tts.id), bbox_inches='tight')

# TTS metrics evaluation
print("TTS metrics evaluation")
dict_filters = tts.metricsEvaluation(os.path.join(tts.path_tts, "%s-metrics.xlsx"%tts.id), evaluate_filtered_metrics) #, dttm="IEC60076_VCS_hotspot"
if not evaluate_filtered_metrics:
    dict_filters = {}

# Plot with filters
if plot_original_ts:
    for f in dict_filters.keys():
        fig = tts.plot(tts.ts_resampled, label_title="Original timeseries - %s filter"%f,
                lst_first_axis=['t_amb', 't_bl', 't_tl', 't_hs'], label_first_axis="Temperature", mpl_options_first_axis={"major_formatter": FormatStrFormatter("%.0f°C")},
                lst_second_axis=['load'], label_second_axis="Load factor", y_lim_second_axis={"bottom": 0, "top": 1.5}, mpl_options_second_axis={"major_formatter": PercentFormatter(xmax=1, decimals=0)},
                lst_third_axis=['tap_pos', 'cooling_stage'], label_third_axis="Tap position | Cooling stage", mpl_options_third_axis={"major_locator": MaxNLocator(integer=True)},
                emphasize_time=dict_filters[f])
        fig.savefig(os.path.join(tts.path_tts, "%s - original timeseries - %s filter.png"%(tts.id, f)), bbox_inches='tight')

# =============================================================================
# Evaluate DTTMs
# =============================================================================
print("\n===== Evaluation =====")

# Results path
path_out = os.path.join(path_tts, "TTS%s"%id_tfo, "evaluations", dt.datetime.now().strftime("%Y-%m-%d"), "TTS%s%s"%(id_tfo, id_input_ds))
if not os.path.exists(path_out):
    os.makedirs(path_out)

lst_dttm_cls = []
# Get all DTTMs in dttm folder
lst_py_files = [f[:-3] for f in os.listdir(path_dttm) if f.endswith('.py') and f != '__init__.py' and f != 'DTTM.py']
for py in lst_py_files:
    mod = __import__('.'.join(["dttm", py]), fromlist=[py])
    classes = [getattr(mod, x) for x in dir(mod) if isinstance(getattr(mod, x), type) and x != 'DTTM' and x != 'DTTMProprietary']
    lst_dttm_cls += classes

if isinstance(lst_dttm, list):
    # Keep only selected DTTMs
    k = 0
    while k < len(lst_dttm_cls):
        if lst_dttm_cls[k].__name__ in lst_dttm:
            k = k + 1
        else:
            lst_dttm_cls.pop(k)

# Check the timeseries
k = 0
while k < len(lst_ts_to_keep_tl):
    if lst_ts_to_keep_tl[k] in tts.ts_resampled.columns:
        k = k + 1
    else:
        print("Timeserie %s is not in the TTS"%lst_ts_to_keep_tl[k])
        lst_ts_to_keep_tl.pop(k)
print("Timeseries used for top-oil evaluation: %s"%lst_ts_to_keep_tl)
k = 0
while k < len(lst_ts_to_keep_hs):
    if lst_ts_to_keep_hs[k] in tts.ts_resampled.columns:
        k = k + 1
    else:
        print("Timeserie %s is not in the TTS"%lst_ts_to_keep_hs[k])
        lst_ts_to_keep_hs.pop(k)
print("Timeseries used for hotspot evaluation: %s"%lst_ts_to_keep_hs)

# Create dttm objects and separate top-liquid and hotspot models
lst_dttm_top_liquid = []
lst_dttm_hotspot = []
print("DTTMs in evaluation:")
for cl in lst_dttm_cls:
    tmp = cl()

    if tmp.type == "top_liquid":
        print("Top-liquid DTTM: %s"%tmp.shortname)
        if not tmp.isEvaluationPossible(tts, lst_ts_to_keep_tl):
            print("=> Evaluation not possible of DTTM %s on TTS %s"%(tmp.shortname, tts.id))
        else:
            lst_dttm_top_liquid.append(tmp)
    elif tmp.type == "hotspot":
        print("Hotspot DTTM: %s"%tmp.shortname)
        if not tmp.isEvaluationPossible(tts, lst_ts_to_keep_hs):
            print("=> Evaluation not possible of DTTM %s on TTS %s"%(tmp.shortname, tts.id))
        else:
            lst_dttm_hotspot.append(tmp)
    else:
        raise Exception("Unsupported DTTM type : %s"%tmp.type)

### Run the evaluation
## Top liquid dttm
if len(lst_dttm_top_liquid) > 0:
    label_title = "Top-liquid DTTM evaluation"
    print("\n=== %s ==="%label_title)
    ts_result_to = tts.ts_resampled[["time", "t_tl"]].copy()

    ts_result_to.rename(columns={'t_tl': 'Measured'}, inplace=True)
    lst_dttm_name = []
    for dttm in lst_dttm_top_liquid:
        tmp_result = dttm.evaluateModel(tts, duration_initial_values=duration_initial_values, lst_ts_to_keep=lst_ts_to_keep_tl)
        if not tmp_result.empty:
            ts_result_to = pd.merge_ordered(ts_result_to, tmp_result[["time", "t_tl_model"]], fill_method=None, on="time")
            ts_result_to.rename(columns={'t_tl_model': dttm.longname}, inplace=True)
            lst_dttm_name.append(dttm.longname)

    # Plot timeseries
    fig = tts.plot(ts_result_to, label_title=label_title,
            lst_first_axis=['t_amb', 'Measured'] + lst_dttm_name, label_first_axis="Temperature", mpl_options_first_axis={"major_formatter": FormatStrFormatter("%.0f°C")},
            lst_second_axis=['load'], label_second_axis="Load factor", y_lim_second_axis={"bottom": 0, "top": 1.5}, mpl_options_second_axis={"major_formatter": PercentFormatter(xmax=1, decimals=0)},
            lst_third_axis=['tap_pos', 'cooling_stage'], label_third_axis="Tap position | Cooling stage", mpl_options_third_axis={"major_locator": MaxNLocator(integer=True)})
    fig.savefig(os.path.join(path_out, "%s - timeseries.png"%label_title), bbox_inches='tight')

    # Save timeseries to Excel
    tts.saveTimeserieToExcel(ts_result_to, os.path.join(path_out, "%s - timeseries.xlsx"%label_title))

    # Differences evaluation
    writer = pd.ExcelWriter(os.path.join(path_out, "%s - error metrics.xlsx"%label_title), engine='xlsxwriter')
    tts.differencesEvaluation(ts_result_to, analysisToPerform=lst_dttm_graphs_to_show, xlsx_writer=writer, label_title=add_graph_title + "unfiltered", path_out=path_out)
    for f in dict_filters.keys():
        ts_filtered = ts_result_to.loc[ts_result_to.time.isin(dict_filters[f])]
        tts.differencesEvaluation(ts_filtered, analysisToPerform=lst_dttm_graphs_to_show, xlsx_writer=writer, label_title=f, path_out=path_out)
    writer.close()
else:
    print("\n=== No top-liquid DTTM to evaluate ===")

## Hotspot dttm
if len(lst_dttm_hotspot) > 0:
    label_title = "Hotspot DTTM evaluation"
    print("\n=== %s ==="%label_title)
    print("Timeseries used: %s"%lst_ts_to_keep_hs)
    ts_result_hs = tts.ts_resampled[["time", "t_hs"]].copy()
    ts_result_hs.rename(columns={'t_hs': 'Measured'}, inplace=True)
    lst_dttm_name = []
    for dttm in lst_dttm_hotspot:
        tmp_result = dttm.evaluateModel(tts, duration_initial_values=duration_initial_values, lst_ts_to_keep=lst_ts_to_keep_hs)
        if not tmp_result.empty:
            ts_result_hs = pd.merge_ordered(ts_result_hs, tmp_result[["time", "t_hs_model"]], fill_method=None, on="time")
            ts_result_hs.rename(columns={'t_hs_model': dttm.longname}, inplace=True)
            lst_dttm_name.append(dttm.longname)

    # Plot timeseries
    fig = tts.plot(ts_result_hs, label_title=label_title,
            lst_first_axis=['t_amb', 't_tl', 'Measured'] + lst_dttm_name, label_first_axis="Temperature", mpl_options_first_axis={"major_formatter": FormatStrFormatter("%.0f°C")},
            lst_second_axis=['load'], label_second_axis="Load factor", y_lim_second_axis={"bottom": 0, "top": 1.5}, mpl_options_second_axis={"major_formatter": PercentFormatter(xmax=1, decimals=0)},
            lst_third_axis=['tap_pos', 'cooling_stage'], label_third_axis="Tap position | Cooling stage", mpl_options_third_axis={"major_locator": MaxNLocator(integer=True)})
    fig.savefig(os.path.join(path_out, "%s - timeseries.png"%label_title), bbox_inches='tight')

    # Save timeseries to Excel
    tts.saveTimeserieToExcel(ts_result_hs, os.path.join(path_out, "%s - timeseries.xlsx"%label_title))

    # Differences evaluation
    writer = pd.ExcelWriter(os.path.join(path_out, "%s - error metrics.xlsx"%label_title), engine='xlsxwriter')
    tts.differencesEvaluation(ts_result_hs, analysisToPerform=["ageing"] + lst_dttm_graphs_to_show, xlsx_writer=writer, label_title=add_graph_title + "unfiltered", path_out=path_out)
    for f in dict_filters.keys():
        ts_filtered = ts_result_hs.loc[ts_result_hs.time.isin(dict_filters[f])]
        tts.differencesEvaluation(ts_filtered, analysisToPerform=["ageing"] + lst_dttm_graphs_to_show, xlsx_writer=writer, label_title=f, path_out=path_out)
    writer.close()
else:
    print("\n=== No hotspot DTTM to evaluate ===")

if show_figures:
    plt.show()
