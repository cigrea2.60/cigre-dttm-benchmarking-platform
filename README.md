# About the project

Dynamic transformer thermal model benchmarking platform (CIGRE DTTM-BP) is an open-source platform which enables transparent, objective, and repeatable simulation, visualization, and the evaluation of the accuracy and reliability of openly published and proprietary DTTMs (Dynamic transformer thermal models).
The CIGRE DTTM-BP is developed and maintained by the CIGRE working group A2.60, with goal of to reviewing the existing standard and state-of-the-art Dynamic Transformer Thermal Model (DTTM), identify and evaluate possible modelling improvements, and propose recommendations for improvement and future revision of the IEC standard 60076-7 DTTM.

# DTTM-BP data structure

The core structure of CIGRE DTTM-BP is based on modular implementation of DTTMs, tools for analysis and visualisation of DTTM error metrics, and unified DTTM input/output data exchange format (in .xlsx format) called TTS (Transformer Thermal Scenario) allowing uniform data manipulation and exchange. Dynamic transformer thermal models (DTTM) describe transformer thermal behaviour and enables simulation, whereas TTS provides the parametrization and driving input data for the DTTM and allows simulation and comparison of multiple combinations of dynamic thermal behaviour responses and multiple ambient and loading scenarios.

![Structure and key elements of the CIGRE Dynamic Transformer Thermal Model - Benchmarking Platform](doc/DTTM-BP-structure.png)

# Dynamic transformer thermal model Benchmarking Platform (DTTM-BP) engine 

The core executable code of the benchmarking platform is called by main_compare.py, which takes care of the data processing and combinational interactions between TTS and DTTMs, enabling transparent implementation of openly published models for the simulation of the dynamic response of the hotspot and top-liquid temperatures. The TTS timeseries are resampled to a commonly selected time base (10 min as the default choice), which is reflected in the count of TTS samples.

## Python requirements

Python 3.9.13. Refer to requirements.txt for the dependencies.

This [wiki page](https://gitlab.com/cigrea2.60/cigre-dttmbp/-/wikis/How-to-install-Python) can be helpful to define your environment.

# Transformer Thermal Scenario (TTS)

TTS provides description of the transformer, model parametrisation, operational and ambiental measurements needed for simulations. TTS data is packed in two datasets: time invariable transformer parameters and variable timeseries, adjoined by a pdf document to document graphical and unstructured data (eg. test report

The TTS contains three types of data:

- transformer physical and geometrical data and (time invariant) open model parameters
- on-line monitored ambient conditions and transformer operation related measurements,
- reference temperature measurements for model validation (top-, -bottom liquid and hotspot)

TTS is also used as the communication

## TTS implementations 

One folder `TTSx` per TTS in `./tts` with:

- Parameters file `TTSx-parameters.xlsx` 
  - Worksheet with parameters default name is `TTS parameters` (it can be set by the argument `ws_params_name` of the class `TTS`.
  - Models name are the one of the DTTM files in folder `./dttm`.
  - Parameters values are accessed in code with `tts.params.model.shortname.value`. When no model is provided in the Excel cell, the parameter will be added to a "fake" model `main`.
  - `tts.params.model.shortname` contains longname, unit, value as defined in the Excel file
  - Other worksheets can be added for description of the TTS for example, they won't be analysed
- Timeseries file `TTSxN-timeseries.xlsx` 
  - Each file represent a dataset for `TTSx`
  - Each worksheet can have a different time sampling rate, the will be merged at the import stage
  - Excel datetime format has to be `yyyy-mm-dd hh:mm:ss,000`. Copy/paste format from one of the existing files for easy application.
  - 'Usual' column names are: 
    - `time`: date and time of the data, with the requested Excel datetime format has to be `yyyy-mm-dd hh:mm:ss,000`
    - `load`: load factor, between 0 and 1 (or > 1 for overload), in reference to the rated power
    - `t_amb`: ambient temperature (cooling medium), °C
    - `t_bl`: bottom liquid temperature, °C
    - `t_tl`: top liquid temperature, °C
    - `t_hs`: winding hotspot temperature, °C
    - `cooling_stage`: cooling stage if variable in operation, minimum value is 0

# Dynamic transformer thermal model 

A DTTM can be "open-source" when the source code is included in this platform, or "proprietary" when the evaluation is performed outside of this platform.

For each DTTM 2 types of outputs (in timeseries format) are defined:

- `top_liquid`: to evaluate the top liquid temperature
- `hotspot`: to evaluate the winding hotspot temperature

### Open-source DTTM implementations

Each open-source DTTM is defined in a file in folder `./dttm`. It must inherit the `DTTM` class and implement method `evaluateModel(tts, initial_value)`.

To implement a new Open-source DTTM, copy/paste an existing one and modify it. 

The DTTM-BP currently includes the standard loading guide models [4, 5, 6], state-of-the-art literature models [7], as well as further developed versions of the standard IEC model enabling modelling of the variable cooling system (VCS) [8] and variable tap position (VTP) [9]. The evaluations are done by exchanging the TTS data files between the DTTM-BP management team and the proprietary DTTM benchmarking participants. The DTTM-BP code is implemented in Python and maintained/distributed via a Git repository. Open access to the Git repository hosting the DTTM-BP is available on Gitlab [10]. The results of proprietary DTTMs and relevant error metrics evaluations and comparisons to the openly published models in the DTTM-BP can be transparently processed as a part of the WG A2.60 DTTM benchmark. Openly published DTTMs from standards and literature are coded in Python processed within the DTTM-BP whereas for proprietary models only the results are processed and visualised within DTTM-BP.

[4] 	IEC standard 60076-7:2018, "Power transformers - Part 7: Loading guide for mineral-oil-immersed power transformers," 2018. [5] 	IEEE Std. C57.91-2011, IEEE Guide for Loading Mineral-Oil-Immersed Transformers and Step-Voltage Regulators, 2011. [6] 	AS/NZS Standard 60076-7:2013, "Power Transformers Loading Guide for Oil-Immersed Power Transformers (IEC 60076-7, Ed. 1.0 (2005) MOD)’.," 2013. [7] 	D. Susa and M. Lehtonen, "Dynamic Thermal Modeling of Power Transformers: Further Development," IEEE Transactions on Power Delivery 21, vol. no. 4, 2006. [8] 	T. Gradnik, X. Zhang, I. Lumpandina, R. Desquiens and P. Picher, "Dynamic thermal modelling of power transformer with variable cooling stages," in to be published at Cigre SC A2 & 6th International Colloquium "Transformer Research and Asset Management", Split, Croatia, 2023. [9] 	A. Portillo, F. Portillo, T. Gradnik, X. Zhang, I. Lumpandina, R. Desquiens and P. Picher, "Dynamic thermal modelling of power transformer with variable tap positions, to be published," in to be published at Cigre SC A2 & 6th International Colloquium "Transformer Research and Asset Management", Split, Croatia, 2023.

### Proprietary DTTMs

Proprietary DTTMs are defined in the file `./dttm/ProprietaryModels.py`. Each class represent a proprietary DTTM.

To implement a new Proprietary DTTM,

1. Copy/paste the example in the file
2. Change the class name 'ExampleProprietaryModel'
3. Change the longname/type_dttm according to the Proprietary DTTM to include

# Code implementation and Definitions

## Proprietary DTTM Model processing

DTTM-BP structure allows inclusion of proprietary DTTMs in the benchmark, for which the source code is not publicly shareable and consequently DTTM evaluations need to be provided (calculated) by the DTTM benchmark participants. To assure objective evaluation of DTTM accuracy and enable comparisons between proprietary and DTTMs, DTTM-BP will use/need two subsets of the DTTM input and reference data per each applicable TTS. Both subsets of the DTTM input data will be shared with the survey contributors for the DTTM training purposes. The first subset of DTTM reference data will be shared for DTTM training whereas the second DTTM reference subset will be kept confidential and will be uncovered after the DTTM benchmarking survey has been, after the proprietary DTTM output datasets are collected.

The goal of benchmarking is to evaluate existing DTTMs and to provide a framework for evaluating future DTTMs. The benchmark is set up to permit the evaluation of openly published as well as proprietary DTTMs. Openly published DTTMs are fully integrated into the platform while proprietary models are secured and handled by its benchmarking participants; only their results are analysed and compared in the DTTM-BP. The initial benchmarking effort is performed in the context of CIGRE Working Group A2.60 in the following stages:

- Stage 1: Data in the form of Transformer Thermal Scenarios (TTS), further described in Section III, are collected from data owners by the benchmarking managing team. TTS data is collected through a survey and stored in a separated repository to secure the reference datasets until the execution of the benchmarking validations.
- Stage 2: Suitable TTS are selected by the benchmark managing team to give a fair representation of DTTM applications with the help of input from the entire A2.60 Working Group.
- Stage 3: Selected TTS are divided in two groups, one for  DTTM parametrization/training (for example, TTSa01 and 02) and the other for validation of DTTM errors (TTSa>02), and then distributed to benchmarking participants. Reference temperature measurements of the top-liquid and winding hotspot are included in training but not validation. The training data can be used for parametrization of DTTMs that are not based on machine learning.
- Stage 4: Benchmarking participants submit computed temperature results from proprietary models to the benchmark managing team, who then compiles and publishes the results. Openly published models are implemented as a part of the platform.
- Stage 5: Full data for the selected TTS including reference temperature measurements for validation, is made available to the public at the conclusion of the benchmarking process.

Have a look at the step-by-step [video](https://youtu.be/VmLoxBwWuoo) demonstrating how the proprietary DTTMs are processed.

After stage 5, it will be possible for any researcher to validate their DTTMs, comparing them to established/standard DTTMs through evaluation of the same DTTM error metrics analysed using the same TTS datasets. Although Stage 5 represents the final stage of the formal benchmarking procedure and its results are to be presented by the of CIGRE Working Group A2.60, it will be possible to initiate a new benchmarking process using the same platform using either existing or newly collected TTS.

When DTTM are evaluated, a Excel files with timeseries and parameters are generated in the folder `./tts/TTSx/proprietary_dttm`. These files has to be sent to the DTTM owner for evaluation.
The results have to be integrated in the empty column of the file `TTSx_evaluation_timeseries_TO_FILL.xlsx` and this file renamed to `TTSx_evaluation_timeseries_FILLED.xlsx` and copied to the folder `./tts/TTSx/proprietary_dttm`.
At the next evaluation of the DTTM, the result file will be processed.

## Run the Benchmark Platform

Have a look at the step-by-step [video](https://gitlab.com/cigrea2.60/cigre-dttm-benchmarking-platform/-/blob/main/doc/DTTM-BP-Gitpod_demo.mp4) demonstrating how to run the CIGRE DTTM-BP python code online on [gitpod.io](https://gitpod.io/).

# License information 

Copyright 2024 Tim GRADNIK and the CIGRE working group A2.60

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details: https://www.gnu.org/licenses/gpl-3.0.html

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission. Copyright holder contact: Tim GRADNIK(tim.gradnik@eimv.si) and DESQUIENS Remi (remi.desquiens@edf.fr).

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.